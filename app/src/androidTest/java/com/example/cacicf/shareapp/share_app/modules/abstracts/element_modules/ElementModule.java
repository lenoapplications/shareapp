package com.example.cacicf.shareapp.share_app.modules.abstracts.element_modules;


import android.support.test.espresso.NoMatchingViewException;
import android.support.test.espresso.ViewAssertion;
import android.support.test.rule.ActivityTestRule;
import android.util.Log;

import com.example.cacicf.shareapp.R;
import com.example.cacicf.shareapp.app.view_layer.user_interface.activites.start_app_activity.StartAppActivity;
import com.example.cacicf.shareapp.components.components_configuration.socket_installer_config.client.Client;
import com.example.cacicf.shareapp.components.components_configuration.socket_installer_config.client.external_context.ExternalContextInitializator;
import com.example.cacicf.shareapp.components.components_configuration.socket_installer_config.client.notificationer.ClientNotificationer;
import com.example.cacicf.shareapp.models.android_models.abstracts.android_manager.view_manager.fragment_view.dialog_fragment_view.DialogFragmentView;
import com.example.cacicf.shareapp.share_app.modules.matchers.ToastMatcher;
import async_communicator.AsyncCommunicator;
import socket_installer.SI_behavior.interfaces.notification.DataTradeModel;

import static android.support.test.espresso.Espresso.*;
import static android.support.test.espresso.matcher.ViewMatchers.*;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isClickable;
import static android.support.test.espresso.matcher.ViewMatchers.withId;


public abstract class ElementModule<A> {
    protected final int elementId;
    private final AsyncCommunicator asyncCommunicator = AsyncCommunicator.getAsyncCommunicator();
    private final ActivityTestRule appActivityActivityTestRule;

    protected ElementModule(ActivityTestRule activityTestRule,int elementId) {
        this.elementId = elementId;
        this.appActivityActivityTestRule = activityTestRule;
    }

    public boolean isElementDisplayed(){
        try {
            onView(withId(elementId)).check(matches(isDisplayed()));
            return true;
        }catch (NoMatchingViewException e){
            return false;
        }
    }

    public void isToastDisplayed(String message){
        onView(withText(message)).inRoot(new ToastMatcher()).check(matches(isDisplayed()));
    }

    public void dismissDialogModule(String tag) throws Throwable {
        appActivityActivityTestRule.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                DialogFragmentView dialogFragmentView = (DialogFragmentView) appActivityActivityTestRule.getActivity().getFragmentManager().findFragmentByTag(tag);
                dialogFragmentView.dismiss();
            }
        });
    }

    public void waitThreadToFinish(long threadId){
        asyncCommunicator.waitThreadForResponse(threadId);
    }
    public void waitForFlag(long threadId,String flagId){
        asyncCommunicator.waitForFlag(threadId,flagId);
    }

}
