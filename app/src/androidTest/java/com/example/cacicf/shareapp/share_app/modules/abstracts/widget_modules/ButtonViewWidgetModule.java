package com.example.cacicf.shareapp.share_app.modules.abstracts.widget_modules;

import com.example.cacicf.shareapp.share_app.modules.abstracts.widget_modules.WidgetModule;

public abstract class ButtonViewWidgetModule extends WidgetModule {
    public ButtonViewWidgetModule(int id) {
        super(id);
    }
}
