package com.example.cacicf.shareapp.share_app.modules.abstracts.widget_modules;

import android.support.test.espresso.UiController;
import android.support.test.espresso.ViewAction;
import android.view.View;
import android.widget.TextView;

import org.hamcrest.Description;
import org.hamcrest.Matcher;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

public abstract class TextViewWidgetModule extends WidgetModule {
    public TextViewWidgetModule(int id) {
        super(id);
    }

    public void setValue(String value){
        onView(withId(widgetId)).check(matches(new Matcher<View>() {
            @Override
            public boolean matches(Object item) {
                return ((View)item) instanceof TextView;
            }

            @Override
            public void describeMismatch(Object item, Description mismatchDescription) {
                mismatchDescription.appendText("View is not instance of TextView");
            }

            @Override
            public void _dont_implement_Matcher___instead_extend_BaseMatcher_() {

            }

            @Override
            public void describeTo(Description description) {
                description.appendText("Checking if view is instance of TextView");
            }
        })).perform(clearText()).perform(typeText(value));
    }

    public String getValue(){
        final String[] value = {null};

        onView(withId(widgetId)).perform(new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return isAssignableFrom(TextView.class);
            }

            @Override
            public String getDescription() {
                return "Value from view";
            }

            @Override
            public void perform(UiController uiController, View view) {
                if (view instanceof TextView){
                    value[0] = ((TextView) view).getText().toString();
                }else{
                    value[0] = "View is not an instance of TextView";
                }
            }
        });
        return value[0];
    }
}
