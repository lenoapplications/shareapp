package com.example.cacicf.shareapp.share_app.modules.abstracts.widget_modules;

import android.support.test.espresso.NoMatchingViewException;
import android.support.test.espresso.action.ViewActions;
import android.util.Log;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isClickable;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

public abstract class WidgetModule {
    protected final int widgetId;

    public WidgetModule(int id){
        widgetId = id;
    }

    public void click(){
        onView(withId(widgetId)).check(matches(isClickable())).perform(ViewActions.click());
    }
    public boolean isWidgetDisplayed(){
        try {
            onView(withId(widgetId)).check(matches(isDisplayed()));
            return true;
        }catch (NoMatchingViewException e){
            return false;
        }
    }
}
