package com.example.cacicf.shareapp.share_app.modules.abstracts.widget_modules.list;

import android.support.test.espresso.ViewAction;
import android.support.test.espresso.contrib.RecyclerViewActions;

import com.example.cacicf.shareapp.R;
import com.example.cacicf.shareapp.models.android_models.abstracts.android_manager.view_manager.adapter_view.recycler_view.RecyclerAdapterView;


import java.util.List;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.CoreMatchers.anything;

public class RecylerViewList<A extends RecyclerAdapterView>{
    private final int listId;
    private final A listAdapter;

    public RecylerViewList(int listId, A listAdapter) {
        this.listId = listId;
        this.listAdapter = listAdapter;
    }


    public void clickOnItem(int position){
        onView(withId(listId)).perform(RecyclerViewActions.actionOnItemAtPosition(position,click()));
    }

    public int getListSize(){
        return listAdapter.getItemCount();
    }

    public <A> List<A> getList(){
        return (List<A>)listAdapter.getCompleteList();
    }

    public void clickOnItemAtPosition(int position){
        onView(withId(listId)).perform(RecyclerViewActions.actionOnItemAtPosition(position,click()));
    }
    public void performActionOnItem(int position, ViewAction viewAction){
        onView(withId(listId)).perform(RecyclerViewActions.actionOnItemAtPosition(position,viewAction));
    }


}
