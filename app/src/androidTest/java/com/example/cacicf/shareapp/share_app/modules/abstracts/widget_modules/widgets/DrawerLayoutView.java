package com.example.cacicf.shareapp.share_app.modules.abstracts.widget_modules.widgets;

import android.support.test.espresso.base.DefaultFailureHandler;
import android.support.test.espresso.contrib.DrawerActions;
import android.support.test.espresso.contrib.DrawerMatchers;
import android.support.v4.widget.DrawerLayout;

import com.example.cacicf.shareapp.R;
import com.example.cacicf.shareapp.share_app.modules.abstracts.widget_modules.LayoutViewModule;
import com.example.cacicf.shareapp.share_app.modules.elements.views.HomeAppNavigationView;

import org.hamcrest.Matcher;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.swipeLeft;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

public class DrawerLayoutView extends LayoutViewModule{

    public HomeAppNavigationView navigationView = new HomeAppNavigationView(R.id.homeApp_activity_navigation_view);

    public DrawerLayoutView(int id) {
        super(id);
    }

    public void swipe(){
        onView(withId(widgetId)).perform(DrawerActions.open());
    }
    public void swipeBack(){
        onView(withId(widgetId)).perform(DrawerActions.close());
    }
    public boolean isOpen(){
        try {
            return navigationView.isWidgetDisplayed();
        }catch (Throwable d){
            return false;
        }
    }
}
