package com.example.cacicf.shareapp.share_app.modules.abstracts.widget_modules.widgets;

import android.support.test.espresso.UiController;
import android.support.test.espresso.ViewAction;
import android.support.test.espresso.matcher.BoundedMatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.cacicf.shareapp.share_app.modules.abstracts.widget_modules.TextViewWidgetModule;

import org.hamcrest.Description;
import org.hamcrest.Matcher;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

public class EditTextViewWidget extends TextViewWidgetModule {
    public EditTextViewWidget(int id) {
        super(id);
    }

    public boolean hasErrorText(){
        final boolean[] hasError = {false};
        onView(withId(widgetId)).perform(new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return isAssignableFrom(TextView.class);
            }

            @Override
            public String getDescription() {
                return "Does edit text has error";
            }

            @Override
            public void perform(UiController uiController, View view) {
                hasError[0] = ((EditText)view).getError() != null;
            }
        });
        return hasError[0];
    }
    public boolean hasErrorText(String errorText){
        final boolean[] hasError = {false};
        onView(withId(widgetId)).perform(new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return isAssignableFrom(TextView.class);
            }

            @Override
            public String getDescription() {
                return "Does edit text has error";
            }

            @Override
            public void perform(UiController uiController, View view) {
                hasError[0] = ((EditText)view).getError().equals(errorText);
            }
        });
        return hasError[0];

    }

}
