package com.example.cacicf.shareapp.share_app.modules.abstracts.widget_modules.widgets;

import android.support.test.espresso.action.ViewActions;

import com.example.cacicf.shareapp.share_app.modules.abstracts.widget_modules.ButtonViewWidgetModule;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

public class MenuItemViewWidget extends ButtonViewWidgetModule {
    private String textItem;

    public MenuItemViewWidget(int id,String textItem) {
        super(id);
        this.textItem = textItem;
    }

    public void clickMenuItem(){
        onView(withText(textItem)).perform(ViewActions.click());
    }
}
