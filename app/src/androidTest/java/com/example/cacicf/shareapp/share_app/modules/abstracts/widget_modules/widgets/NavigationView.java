package com.example.cacicf.shareapp.share_app.modules.abstracts.widget_modules.widgets;

import android.support.test.espresso.contrib.DrawerActions;

import com.example.cacicf.shareapp.R;
import com.example.cacicf.shareapp.share_app.modules.abstracts.widget_modules.LayoutViewModule;

import java.util.ArrayList;
import java.util.List;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.swipeLeft;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

public class NavigationView extends LayoutViewModule {

    public NavigationView(int id) {
        super(id);
    }

}
