package com.example.cacicf.shareapp.share_app.modules.elements.activities;

import android.support.test.rule.ActivityTestRule;

import com.example.cacicf.shareapp.R;
import com.example.cacicf.shareapp.share_app.modules.abstracts.element_modules.ElementModule;
import com.example.cacicf.shareapp.share_app.modules.abstracts.widget_modules.widgets.DrawerLayoutView;
import com.example.cacicf.shareapp.share_app.modules.elements.fragments.ShareFragmentElement;

public class HomeAppActivityElement extends ElementModule {
    public final DrawerLayoutView drawerLayoutView = new DrawerLayoutView(R.id.homeApp_activity_drawer_layout);
    public final ShareFragmentElement shareFragmentElement;

    public HomeAppActivityElement(int elementId,ActivityTestRule activityTestRule) {
        super(activityTestRule, elementId);
        shareFragmentElement = new ShareFragmentElement(activityTestRule,R.id.homeApp_activity_shareFragment_head_bar);
    }
}
