package com.example.cacicf.shareapp.share_app.modules.elements.activities;

import android.support.test.rule.ActivityTestRule;
import android.support.v7.widget.RecyclerView;

import com.example.cacicf.shareapp.R;
import com.example.cacicf.shareapp.app.service_layer.notification_classes.connection_notifications.ConnectionNotifications;
import com.example.cacicf.shareapp.app.view_layer.user_interface.activites.start_app_activity.ServerListAdapter;
import com.example.cacicf.shareapp.models.data_objects_holder.recylcer_view_objects.start_app_server_list.DataServerObjectHolder;
import com.example.cacicf.shareapp.share_app.modules.abstracts.element_modules.ElementModule;
import com.example.cacicf.shareapp.share_app.modules.abstracts.widget_modules.list.RecylerViewList;
import com.example.cacicf.shareapp.share_app.modules.abstracts.widget_modules.widgets.ButtonWidget;
import com.example.cacicf.shareapp.share_app.modules.elements.fragment_dialogs.AddServerDialogFragmentElement;

import static com.example.cacicf.shareapp.share_app.statics.static_fields.StaticFields.SERVER_HOST;

public class StartAppActivityElement extends ElementModule {
    public AddServerDialogFragmentElement addServerDialogFragment;
    public ButtonWidget addServerButton = new ButtonWidget(R.id.startApp_activity_button_addServer);
    public ButtonWidget removeServerButton = new ButtonWidget(R.id.startApp_activity_button_deleteServers);
    public RecylerViewList<ServerListAdapter> recylerViewList;

    public StartAppActivityElement(int elementId, ActivityTestRule activityTestRule) {
        super(activityTestRule,elementId);
        initRecylerViewList(activityTestRule);
        addServerDialogFragment = new AddServerDialogFragmentElement(R.id.startApp_dialogFragmentAddServer_layout_head,activityTestRule);
    }

    private void initRecylerViewList(ActivityTestRule activityTestRule) {
        try {
            activityTestRule.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ServerListAdapter serverListAdapter = (ServerListAdapter) ((RecyclerView)activityTestRule.getActivity().findViewById(R.id.startApp_activity_recyclerView_serverList)).getAdapter();
                    recylerViewList = new RecylerViewList<>(R.id.startApp_activity_recyclerView_serverList,serverListAdapter);
                }
            });
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    public void setupForHomeActivitySetup() throws Throwable {
        String serverName = "Test";
        addServerButton.click();
        addServerDialogFragment.editTextIpAddress.setValue(SERVER_HOST);
        addServerDialogFragment.editTextServerName.setValue(serverName);
        addServerDialogFragment.buttonSaveIp.click();
        waitThreadToFinish(ConnectionNotifications.ID_THREAD_FINISHED);
        dismissDialogModule("ADD_SERVER_DIALOG_FRAGMENT");
    }
}
