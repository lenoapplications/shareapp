package com.example.cacicf.shareapp.share_app.modules.elements.fragment_dialogs;

import android.support.test.rule.ActivityTestRule;

import com.example.cacicf.shareapp.R;
import com.example.cacicf.shareapp.share_app.modules.abstracts.element_modules.ElementModule;
import com.example.cacicf.shareapp.share_app.modules.abstracts.widget_modules.ButtonViewWidgetModule;
import com.example.cacicf.shareapp.share_app.modules.abstracts.widget_modules.widgets.ButtonWidget;
import com.example.cacicf.shareapp.share_app.modules.abstracts.widget_modules.widgets.EditTextViewWidget;

public class AddServerDialogFragmentElement extends ElementModule{

    public final EditTextViewWidget editTextServerName = new EditTextViewWidget(R.id.startApp_dialogFragmentAddServer_editText_serverName);
    public final EditTextViewWidget editTextIpAddress = new EditTextViewWidget(R.id.startApp_dialogFragmentAddServer_editText_ipAddress);
    public final ButtonWidget buttonSaveIp = new ButtonWidget(R.id.startApp_dialogFragmentAddServer_button_saveIpAdress);

    public AddServerDialogFragmentElement(int elementId, ActivityTestRule activityTestRule) {
        super(activityTestRule,elementId);
    }
}
