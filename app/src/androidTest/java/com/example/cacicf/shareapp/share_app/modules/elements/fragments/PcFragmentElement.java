package com.example.cacicf.shareapp.share_app.modules.elements.fragments;

import android.app.Fragment;
import android.support.test.espresso.NoMatchingViewException;
import android.support.test.espresso.ViewAssertion;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.intent.Intents.init;
import static org.assertj.core.api.Assertions.*;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.assertThat;
import static android.support.test.espresso.matcher.ViewMatchers.hasErrorText;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

import com.example.cacicf.shareapp.R;
import com.example.cacicf.shareapp.app.view_layer.user_interface.fragments.home_fragments.share_fragment.pc_fragment.PcFilesListAdapter;
import com.example.cacicf.shareapp.share_app.modules.abstracts.element_modules.ElementModule;
import com.example.cacicf.shareapp.share_app.modules.abstracts.widget_modules.list.RecylerViewList;
import com.example.cacicf.shareapp.share_app.modules.abstracts.widget_modules.widgets.ButtonWidget;

import java.util.List;

public class PcFragmentElement extends ElementModule {
    public RecylerViewList<PcFilesListAdapter> recylerViewList;
    public ButtonWidget buttonWidget = new ButtonWidget(R.id.homeApp_activity_shareFragment_pcFragment_filesPreview_layout_list_actionBarLayout_imageButton_goBack);

    public PcFragmentElement(ActivityTestRule activityTestRule, int elementId) {
        super(activityTestRule, elementId);
    }

    public void initRecylerViewList() {
        try {

            onView(withId(R.id.homeApp_activity_shareFragment_pcFragment_filesPreview_layout_list_recyclerView)).check(new ViewAssertion() {
                @Override
                public void check(View view, NoMatchingViewException noViewFoundException) {
                    RecyclerView recyclerView = (RecyclerView) view;
                    recylerViewList = new RecylerViewList<>(R.id.homeApp_activity_shareFragment_pcFragment_filesPreview_layout_list_recyclerView,
                            (PcFilesListAdapter)recyclerView.getAdapter());
                }
            });
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }
}
