package com.example.cacicf.shareapp.share_app.modules.elements.fragments;

import android.support.test.rule.ActivityTestRule;

import com.example.cacicf.shareapp.share_app.modules.abstracts.element_modules.ElementModule;

public class PhoneFragmentElement extends ElementModule {


    public PhoneFragmentElement(ActivityTestRule activityTestRule, int elementId) {
        super(activityTestRule, elementId);
    }
}
