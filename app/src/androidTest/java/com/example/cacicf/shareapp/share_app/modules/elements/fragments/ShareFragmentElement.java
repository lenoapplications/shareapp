package com.example.cacicf.shareapp.share_app.modules.elements.fragments;

import android.support.test.rule.ActivityTestRule;

import com.example.cacicf.shareapp.R;
import com.example.cacicf.shareapp.share_app.modules.abstracts.element_modules.ElementModule;
import com.example.cacicf.shareapp.share_app.modules.abstracts.widget_modules.widgets.ButtonWidget;

public class ShareFragmentElement extends ElementModule {

    public final ButtonWidget pcButton = new ButtonWidget(R.id.homeApp_activity_shareFragment_head_bar_button_pc);
    public final ButtonWidget phoneButton = new ButtonWidget(R.id.homeApp_activity_shareFragment_head_bar_button_phone);
    public final PcFragmentElement pcFragmentElement;
    public final PhoneFragmentElement phoneFragmentElement;

    public ShareFragmentElement(ActivityTestRule activityTestRule, int elementId) {
        super(activityTestRule, elementId);
        pcFragmentElement = new PcFragmentElement(activityTestRule,R.id.homeApp_activity_shareFragment_pcFragment_filesPreview_layout_list);
        phoneFragmentElement = new PhoneFragmentElement(activityTestRule,2);
    }

}
