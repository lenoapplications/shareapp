package com.example.cacicf.shareapp.share_app.modules.elements.views;

import com.example.cacicf.shareapp.R;
import com.example.cacicf.shareapp.share_app.modules.abstracts.widget_modules.widgets.MenuItemViewWidget;
import com.example.cacicf.shareapp.share_app.modules.abstracts.widget_modules.widgets.NavigationView;

public class HomeAppNavigationView extends NavigationView{
    public MenuItemViewWidget share = new MenuItemViewWidget(R.id.homeApp_activity_navBar_share,"Share");

    public HomeAppNavigationView(int id) {
        super(id);
    }

}
