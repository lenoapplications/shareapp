package com.example.cacicf.shareapp.share_app.statics.static_fields;

public class StaticFields {
    //public static final String SERVER_HOST = "192.168.5.11";
    public static final String SERVER_HOST = "192.168.5.15";
    public static final String CLIENT_HOST = "192.168.5.13";
    public static final int PORT = 3000;
    public static final int TIMEOUT = 1000;
}
