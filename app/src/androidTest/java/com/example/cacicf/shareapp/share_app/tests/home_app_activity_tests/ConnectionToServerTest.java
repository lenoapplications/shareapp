package com.example.cacicf.shareapp.share_app.tests.home_app_activity_tests;

import android.content.Intent;
import android.support.test.espresso.UiController;
import android.support.test.espresso.ViewAction;
import android.support.test.espresso.intent.Intents;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.View;
import android.widget.Button;

import com.example.cacicf.shareapp.R;
import com.example.cacicf.shareapp.app.view_layer.user_interface.activites.home_app_activity.HomeAppActivity;
import com.example.cacicf.shareapp.app.view_layer.user_interface.activites.start_app_activity.StartAppActivity;
import com.example.cacicf.shareapp.share_app.modules.abstracts.widget_modules.widgets.ButtonWidget;
import com.example.cacicf.shareapp.share_app.modules.elements.activities.HomeAppActivityElement;
import com.example.cacicf.shareapp.share_app.modules.elements.activities.StartAppActivityElement;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.intent.Intents.init;
import static org.assertj.core.api.Assertions.*;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.assertThat;
import static android.support.test.espresso.matcher.ViewMatchers.hasErrorText;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.example.cacicf.shareapp.share_app.statics.static_fields.StaticFields.SERVER_HOST;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static org.hamcrest.core.IsAnything.anything;

import org.assertj.core.api.Assertions;
import org.hamcrest.Matcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
public class ConnectionToServerTest {
    @Rule
    public ActivityTestRule<StartAppActivity> appActivityActivityTestRule = new ActivityTestRule<>(StartAppActivity.class);

    @Rule
    public ActivityTestRule<HomeAppActivity> homeAppActivityActivityTestRule = new ActivityTestRule<>(HomeAppActivity.class);

    private HomeAppActivityElement homeAppActivityElement;
    private StartAppActivityElement startAppActivityElement;


    @Before
    public void before() throws Throwable {
        homeAppActivityElement = new HomeAppActivityElement(R.layout.home_app_activity_phones,homeAppActivityActivityTestRule);
        startAppActivityElement = new StartAppActivityElement(R.layout.start_app_activity_phones,appActivityActivityTestRule);
        startAppActivityElement.setupForHomeActivitySetup();
        Intents.init();
    }

    @Test
    public void checkIfHomeActivityIsDisplayed() throws InterruptedException {
        startAppActivityElement.recylerViewList.performActionOnItem(0, new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return null;
            }

            @Override
            public String getDescription() {
                return null;
            }

            @Override
            public void perform(UiController uiController, View view) {
                Button connect = (Button) view.findViewById(R.id.startApp_activity_recyclerViewButton_card_connect);
                connect.performClick();
            }
        });
        intended(hasComponent(HomeAppActivity.class.getName()));
    }


    @After
    public void after(){
        Intents.release();
        homeAppActivityActivityTestRule.finishActivity();
    }


}
