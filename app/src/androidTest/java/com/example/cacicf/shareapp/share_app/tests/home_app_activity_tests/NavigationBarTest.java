package com.example.cacicf.shareapp.share_app.tests.home_app_activity_tests;


import android.support.test.espresso.UiController;
import android.support.test.espresso.ViewAction;
import android.support.test.espresso.base.DefaultFailureHandler;
import android.support.test.espresso.intent.Intents;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.View;
import android.widget.Button;

import com.example.cacicf.shareapp.R;
import com.example.cacicf.shareapp.app.view_layer.user_interface.activites.home_app_activity.HomeAppActivity;
import com.example.cacicf.shareapp.app.view_layer.user_interface.activites.start_app_activity.StartAppActivity;
import com.example.cacicf.shareapp.share_app.modules.elements.activities.HomeAppActivityElement;
import com.example.cacicf.shareapp.share_app.modules.elements.activities.StartAppActivityElement;

import org.assertj.core.api.Assertions;
import org.hamcrest.Matcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;

@RunWith(AndroidJUnit4.class)
public class NavigationBarTest {

    @Rule
    public ActivityTestRule<StartAppActivity> appActivityActivityTestRule = new ActivityTestRule<>(StartAppActivity.class);

    @Rule
    public ActivityTestRule<HomeAppActivity> homeAppActivityActivityTestRule = new ActivityTestRule<>(HomeAppActivity.class);

    private HomeAppActivityElement homeAppActivityElement;
    private StartAppActivityElement startAppActivityElement;


    @Before
    public void before() throws Throwable {
        homeAppActivityElement = new HomeAppActivityElement(R.layout.home_app_activity_phones,homeAppActivityActivityTestRule);
        startAppActivityElement = new StartAppActivityElement(R.layout.start_app_activity_phones,appActivityActivityTestRule);
        startAppActivityElement.setupForHomeActivitySetup();
        Intents.init();
        startAppActivityElement.recylerViewList.performActionOnItem(0, new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return null;
            }

            @Override
            public String getDescription() {
                return null;
            }

            @Override
            public void perform(UiController uiController, View view) {
                Button connect = (Button) view.findViewById(R.id.startApp_activity_recyclerViewButton_card_connect);
                connect.performClick();
            }
        });
        intended(hasComponent(HomeAppActivity.class.getName()));
    }

    @Test
    public void checkIfSwipeWillDisplayNavigationBar(){
        homeAppActivityElement.drawerLayoutView.swipe();
        Assertions.assertThat(homeAppActivityElement.drawerLayoutView.isOpen()).isEqualTo(true);
    }

    @Test
    public void checkIfFragmentForSharingFileOpensWhenClickedOnShare() throws InterruptedException {
        homeAppActivityElement.drawerLayoutView.swipe();
        homeAppActivityElement.drawerLayoutView.navigationView.share.clickMenuItem();
        Assertions.assertThat(homeAppActivityElement.shareFragmentElement.isElementDisplayed()).isEqualTo(true);
    }

    @Test
    public void checkIfNavigationBarClosesWhenItemIsClicked() throws InterruptedException {
        homeAppActivityElement.drawerLayoutView.swipe();
        homeAppActivityElement.drawerLayoutView.navigationView.share.clickMenuItem();
        Assertions.assertThat(homeAppActivityElement.drawerLayoutView.isOpen()).isEqualTo(false);
    }



    @After
    public void after(){
        Intents.release();
        appActivityActivityTestRule.finishActivity();
    }

}
