package com.example.cacicf.shareapp.share_app.tests.home_app_activity_tests;

import android.support.test.espresso.UiController;
import android.support.test.espresso.ViewAction;
import android.support.test.espresso.intent.Intents;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.cacicf.shareapp.R;
import com.example.cacicf.shareapp.app.view_layer.user_interface.activites.home_app_activity.HomeAppActivity;
import com.example.cacicf.shareapp.app.view_layer.user_interface.activites.start_app_activity.StartAppActivity;
import com.example.cacicf.shareapp.models.data_objects_holder.recylcer_view_objects.home_app_lists.share_fragment_lists.FileSystemObjectsHolder;
import com.example.cacicf.shareapp.share_app.modules.elements.activities.HomeAppActivityElement;
import com.example.cacicf.shareapp.share_app.modules.elements.activities.StartAppActivityElement;
import com.example.cacicf.shareapp.share_app.statics.annotations.Repeat;

import org.assertj.core.api.Assertions;
import org.hamcrest.Matcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static com.example.cacicf.shareapp.share_app.statics.static_methods.StaticMethods.sleep;

@RunWith(AndroidJUnit4.class)

public class PcFragmentTest {

    @Rule
    public ActivityTestRule<StartAppActivity> appActivityActivityTestRule = new ActivityTestRule<>(StartAppActivity.class);

    @Rule
    public ActivityTestRule<HomeAppActivity> homeAppActivityActivityTestRule = new ActivityTestRule<>(HomeAppActivity.class);

    private HomeAppActivityElement homeAppActivityElement;
    private StartAppActivityElement startAppActivityElement;

    @Before
    public void before() throws Throwable {
        homeAppActivityElement = new HomeAppActivityElement(R.layout.home_app_activity_phones,homeAppActivityActivityTestRule);
        startAppActivityElement = new StartAppActivityElement(R.layout.start_app_activity_phones,appActivityActivityTestRule);
        startAppActivityElement.setupForHomeActivitySetup();

        Intents.init();
        startAppActivityElement.recylerViewList.performActionOnItem(0, new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return null;
            }

            @Override
            public String getDescription() {
                return null;
            }

            @Override
            public void perform(UiController uiController, View view) {
                Button connect = (Button) view.findViewById(R.id.startApp_activity_recyclerViewButton_card_connect);
                connect.performClick();
            }
        });
        intended(hasComponent(HomeAppActivity.class.getName()));

        homeAppActivityElement.drawerLayoutView.swipe();
        homeAppActivityElement.drawerLayoutView.navigationView.share.clickMenuItem();
        homeAppActivityElement.shareFragmentElement.pcButton.click();
        homeAppActivityElement.shareFragmentElement.pcFragmentElement.initRecylerViewList();
    }

    @Test
    public void checkIfRootIsSendFromServer_MAC_VERSION(){
        List<FileSystemObjectsHolder> fileSystemObjectsList = homeAppActivityElement.shareFragmentElement.pcFragmentElement.recylerViewList.getList();
        Assertions.assertThat(fileSystemObjectsList.get(0).getFileName().equals("/")).isEqualTo(true);
    }

    @Test
    public void checkIfRootIsMarkedAsDirectory(){
        List<FileSystemObjectsHolder> fileSystemObjectsList = homeAppActivityElement.shareFragmentElement.pcFragmentElement.recylerViewList.getList();
        Assertions.assertThat(fileSystemObjectsList.get(0).isDirectory()).isEqualTo(true);
    }

    @Test
    public void checkWhenClickedOnRootNewFilesAreDisplayed(){
        homeAppActivityElement.shareFragmentElement.pcFragmentElement.recylerViewList.performActionOnItem(0, new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return null;
            }

            @Override
            public String getDescription() {
                return "Open root folder";
            }

            @Override
            public void perform(UiController uiController, View view) {
                Assertions.assertThat(homeAppActivityElement.shareFragmentElement.pcFragmentElement.recylerViewList.getListSize()).isEqualTo(1);
                view.performClick();
                Assertions.assertThat(homeAppActivityElement.shareFragmentElement.pcFragmentElement.recylerViewList.getListSize()).isGreaterThan(2);
            }
        });
    }

    @Test
    public void checkWhenClickBackIsReturningUsToRoot(){
        homeAppActivityElement.shareFragmentElement.pcFragmentElement.recylerViewList.performActionOnItem(0, new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return null;
            }

            @Override
            public String getDescription() {
                return "Open root folder";
            }

            @Override
            public void perform(UiController uiController, View view) {
                Assertions.assertThat(homeAppActivityElement.shareFragmentElement.pcFragmentElement.recylerViewList.getListSize()).isEqualTo(1);
                view.performClick();
                Assertions.assertThat(homeAppActivityElement.shareFragmentElement.pcFragmentElement.recylerViewList.getListSize()).isGreaterThan(2);
            }
        });
        homeAppActivityElement.shareFragmentElement.pcFragmentElement.buttonWidget.click();
        Assertions.assertThat(homeAppActivityElement.shareFragmentElement.pcFragmentElement.recylerViewList.getListSize()).isEqualTo(1);
    }

    @After
    public void after(){
        Intents.release();
    }
}
