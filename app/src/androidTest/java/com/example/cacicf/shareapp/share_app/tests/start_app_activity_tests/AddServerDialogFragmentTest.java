package com.example.cacicf.shareapp.share_app.tests.start_app_activity_tests;

import android.support.test.espresso.Espresso;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;
import com.example.cacicf.shareapp.R;
import com.example.cacicf.shareapp.app.service_layer.notification_classes.connection_notifications.ConnectionNotifications;
import com.example.cacicf.shareapp.app.view_layer.user_interface.activites.start_app_activity.StartAppActivity;
import com.example.cacicf.shareapp.models.android_models.abstracts.android_manager.view_manager.fragment_view.dialog_fragment_view.DialogFragmentView;
import com.example.cacicf.shareapp.models.data_objects_holder.recylcer_view_objects.start_app_server_list.DataServerObjectHolder;
import com.example.cacicf.shareapp.share_app.modules.elements.activities.StartAppActivityElement;
import com.example.cacicf.shareapp.share_app.modules.matchers.ToastMatcher;
import org.assertj.core.api.Assertions;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import java.util.List;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.assertThat;
import static android.support.test.espresso.matcher.ViewMatchers.hasErrorText;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.example.cacicf.shareapp.share_app.statics.static_fields.StaticFields.SERVER_HOST;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.assertj.core.api.Assertions.*;

@RunWith(AndroidJUnit4.class)
public class AddServerDialogFragmentTest {

    @Rule
    public ActivityTestRule<StartAppActivity> appActivityActivityTestRule = new ActivityTestRule<>(StartAppActivity.class);

    private StartAppActivityElement startAppActivityElement = new StartAppActivityElement(R.layout.start_app_activity_phones,appActivityActivityTestRule);

    @Before
    public void before(){
        startAppActivityElement = new StartAppActivityElement(R.layout.start_app_activity_phones,appActivityActivityTestRule);
        startAppActivityElement.addServerButton.click();
    }

    @Test
    public void checkIfFragmentIsDisplayed(){
        Assertions.assertThat(startAppActivityElement.addServerDialogFragment.isElementDisplayed()).isEqualTo(true);
    }



    @Test
    public void checkIfUnkownHostErroMessageIsDisplayed() throws InterruptedException {
        startAppActivityElement.addServerDialogFragment.editTextIpAddress.setValue("192.3333");
        startAppActivityElement.addServerDialogFragment.editTextServerName.setValue("Test");
        Espresso.closeSoftKeyboard();
        startAppActivityElement.addServerDialogFragment.buttonSaveIp.click();
        Assertions.assertThat(startAppActivityElement.addServerDialogFragment.editTextIpAddress.hasErrorText()).isEqualTo(true);
    }
    @Test
    public void checkIfErrorMessagesWhenFieldsAreEmptyAreDisplayed(){
        String fieldMustNotBeEmpty = appActivityActivityTestRule.getActivity().getString(R.string.startAppActivity_fragmentAddServer_errors_fieldMustNotBeEmpty);
        startAppActivityElement.addServerDialogFragment.buttonSaveIp.click();

        Assertions.assertThat(startAppActivityElement.addServerDialogFragment.editTextIpAddress.hasErrorText(fieldMustNotBeEmpty)).isEqualTo(true);
        Assertions.assertThat(startAppActivityElement.addServerDialogFragment.editTextServerName.hasErrorText(fieldMustNotBeEmpty)).isEqualTo(true);
    }

    @Test
    public void checkIfToastMesssageIsDisplayedWhenServerIsOffline(){
        String host = "192.168.5.17";
        String toastMessage = appActivityActivityTestRule.getActivity().getString(R.string.startAppActivity_fragmentAddServer_errors_default,host);
        startAppActivityElement.addServerDialogFragment.editTextIpAddress.setValue(host);
        startAppActivityElement.addServerDialogFragment.editTextServerName.setValue("Test");
        startAppActivityElement.addServerDialogFragment.buttonSaveIp.click();

        startAppActivityElement.isToastDisplayed(toastMessage);
    }
    @Test
    public void checkIfServerIsSaved() throws Throwable {
        String serverName = "Test";
        startAppActivityElement.addServerDialogFragment.editTextIpAddress.setValue(SERVER_HOST);
        startAppActivityElement.addServerDialogFragment.editTextServerName.setValue(serverName);
        startAppActivityElement.addServerDialogFragment.buttonSaveIp.click();

        startAppActivityElement.waitThreadToFinish(ConnectionNotifications.ID_THREAD_FINISHED);
        startAppActivityElement.dismissDialogModule("ADD_SERVER_DIALOG_FRAGMENT");

        List<DataServerObjectHolder> list = startAppActivityElement.recylerViewList.getList();

        final boolean[] found = {false};
        list.forEach(server->{
            if (server.getServerName().equals(serverName)){
                found[0] = true;
            }
        });
        Assertions.assertThat(found[0]).isEqualTo(true);
    }

    @After
    public void after() throws Throwable {
        appActivityActivityTestRule.finishActivity();

    }
}
