package com.example.cacicf.shareapp.share_app.tests.start_app_activity_tests;

import android.support.test.rule.ActivityTestRule;

import com.example.cacicf.shareapp.R;
import com.example.cacicf.shareapp.app.service_layer.notification_classes.connection_notifications.ConnectionNotifications;
import com.example.cacicf.shareapp.app.view_layer.user_interface.activites.start_app_activity.StartAppActivity;
import com.example.cacicf.shareapp.app.view_layer.user_interface.fragments.dialog_fragments.loading_dialog_fragments.BasicLoadingDialogFragment;
import com.example.cacicf.shareapp.models.data_objects_holder.recylcer_view_objects.start_app_server_list.DataServerObjectHolder;
import com.example.cacicf.shareapp.share_app.modules.elements.activities.StartAppActivityElement;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.List;

import static com.example.cacicf.shareapp.share_app.statics.static_fields.StaticFields.SERVER_HOST;
import static com.example.cacicf.shareapp.share_app.statics.static_methods.StaticMethods.sleep;
import static org.assertj.core.api.Assertions.assertThat;

public class ServerListAdapterTest {

    @Rule
    public ActivityTestRule<StartAppActivity> appActivityActivityTestRule = new ActivityTestRule<>(StartAppActivity.class);

    private StartAppActivityElement startAppActivityElement;

    @Before
    public void before(){
        startAppActivityElement = new StartAppActivityElement(R.layout.start_app_activity_phones,appActivityActivityTestRule);
    }

    @Test
    public void checkIfServerIsAddedToList() throws Throwable {
        String serverName = "Test";
        startAppActivityElement.addServerButton.click();
        startAppActivityElement.addServerDialogFragment.editTextIpAddress.setValue(SERVER_HOST);
        startAppActivityElement.addServerDialogFragment.editTextServerName.setValue(serverName);
        startAppActivityElement.addServerDialogFragment.buttonSaveIp.click();

        startAppActivityElement.waitThreadToFinish(ConnectionNotifications.ID_THREAD_FINISHED);
        startAppActivityElement.dismissDialogModule("ADD_SERVER_DIALOG_FRAGMENT");

        List<DataServerObjectHolder> list = startAppActivityElement.recylerViewList.getList();

        final boolean[] found = {false};
        list.forEach(server->{
            if (server.getServerName().equals(serverName)){
                found[0] = true;
            }
        });
        assertThat(found[0]).isEqualTo(true);
    }



    @Test
    public void checkIfServerIsRemovedFromList01() throws Throwable {
        String[] serverNames = new String[]{"test0","test1","test2","test3","test4"};
        //String[] ipAddress = new String[]{"192.168.5.17","192.168.5.18","192.168.5.19","192.168.5.20","192.168.5.21"};

        startAppActivityElement.addServerButton.click();
        for (int i = 0;i < serverNames.length; ++i ){
            startAppActivityElement.addServerDialogFragment.editTextIpAddress.setValue(SERVER_HOST);
            startAppActivityElement.addServerDialogFragment.editTextServerName.setValue(serverNames[i]);
            startAppActivityElement.addServerDialogFragment.buttonSaveIp.click();
            startAppActivityElement.waitForFlag(BasicLoadingDialogFragment.THREAD_LOADING,BasicLoadingDialogFragment.DIALOG_DISMISS);
        }
        startAppActivityElement.dismissDialogModule("ADD_SERVER_DIALOG_FRAGMENT");

        List<DataServerObjectHolder> list = startAppActivityElement.recylerViewList.getList();


        final int[] position = {0};
        final int[] count = {0};
        list.forEach(server->{
            if (server.getServerName().equals(serverNames[position[0]])){
                ++count[0];
            }
            ++position[0];
        });
        assertThat(count[0]).isEqualTo(serverNames.length);

        startAppActivityElement.recylerViewList.clickOnItemAtPosition(2);
        startAppActivityElement.recylerViewList.clickOnItemAtPosition(3);
        startAppActivityElement.removeServerButton.click();


        int counter = 0;
        for(int i = 0; i < serverNames.length; ++i) {
            String name = serverNames[i];
            for (int j = 0; j < list.size(); ++j) {
                if (list.get(j).getServerName().equals(name)) {
                    ++counter;
                }
            }
        }
        assertThat(counter).isEqualTo(serverNames.length-2);

    }
}
