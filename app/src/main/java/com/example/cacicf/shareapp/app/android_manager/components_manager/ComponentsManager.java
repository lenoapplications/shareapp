package com.example.cacicf.shareapp.app.android_manager.components_manager;

import com.example.cacicf.shareapp.components.components_configuration.socket_installer_config.client.Client;
import com.example.cacicf.shareapp.models.helper_models.StaticClass;

import async_communicator.AsyncCommunicator;


public class ComponentsManager extends StaticClass {
    private static ComponentsManager componentsManager;
    private final Client client;

    private ComponentsManager(){
        client = new Client();
    }

    public static ComponentsManager getComponentsManager(){
        if (componentsManager == null){
            componentsManager = new ComponentsManager();
        }
        return componentsManager;
    }
    public static void destroyComponentManager(){
        componentsManager.destroy();
    }

    public Client getClient() {
        return client;
    }


    @Override
    public void destroy() {
        componentsManager = null;
    }
}
