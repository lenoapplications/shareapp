package com.example.cacicf.shareapp.app.android_manager.view_manager;


import android.util.Log;

import com.example.cacicf.shareapp.app.android_manager.view_manager.view_size_handler.font_size_handler.FontSizeHandler;
import com.example.cacicf.shareapp.app.android_manager.view_manager.view_size_handler.widget_size_handler.WidgetSizeHandler;
import com.example.cacicf.shareapp.app.android_manager.view_manager.view_size_handler.window_size_handler.WindowSizeHandler;
import com.example.cacicf.shareapp.models.android_models.interfaces.android_manager.view_manager.view_setup_model.ViewSetupModel;
import com.example.cacicf.shareapp.models.helper_models.StaticClass;

public class ViewManager extends StaticClass{
    private static ViewManager viewManager;
    private FontSizeHandler fontSizeHandler;
    private WindowSizeHandler windowSizeHandler;
    private WidgetSizeHandler widgetSizeHandler;

    private ViewManager(){
        initViewHandlerObjects();
    }

    private void initViewHandlerObjects(){
        try {
            fontSizeHandler = new FontSizeHandler();
            windowSizeHandler = new WindowSizeHandler();
            widgetSizeHandler = new WidgetSizeHandler();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static ViewManager getViewManager(){
        if (viewManager == null){
            viewManager = new ViewManager();
        }
        return viewManager;
    }
    public static void destroyViewManager(){
        viewManager.destroy();
    }

    public void setupView(ViewSetupModel viewSetupModel){
        viewSetupModel.setupView(fontSizeHandler,widgetSizeHandler,windowSizeHandler);
    }


    @Override
    public void destroy() {
        viewManager = null;
        fontSizeHandler = null;
        widgetSizeHandler = null;
        widgetSizeHandler = null;
    }
}
