package com.example.cacicf.shareapp.app.android_manager.view_manager.async_display_handler;

import android.app.FragmentManager;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.example.cacicf.shareapp.app.view_layer.user_interface.fragments.dialog_fragments.loading_dialog_fragments.BasicLoadingDialogFragment;
import com.example.cacicf.shareapp.models.android_models.abstracts.android_manager.view_manager.activity_view.ActivityView;
import com.example.cacicf.shareapp.models.android_models.abstracts.android_manager.view_manager.fragment_view.dialog_fragment_view.DialogFragmentView;
import com.example.cacicf.shareapp.models.android_models.interfaces.android_manager.view_manager.view_setup_model.main_async_display_handler.HandlerRunMethod;

import async_communicator.AsyncCommunicator;

public class AsyncLoadingDisplayHandler {

    private BasicLoadingDialogFragment basicLoadingDialogFragment;


    protected AsyncLoadingDisplayHandler() {

    }

    public void activateBasicLoadingDialogFragment(FragmentManager fragmentManager){
        basicLoadingDialogFragment = new BasicLoadingDialogFragment();
        basicLoadingDialogFragment.showLoading(fragmentManager);
    }
    public void deactivateBasicLoadingDialogFragment(){
        basicLoadingDialogFragment.dismiss();
        basicLoadingDialogFragment = null;
    }

    public boolean testPurposeCheckLoading(){
        return basicLoadingDialogFragment == null;
    }

}
