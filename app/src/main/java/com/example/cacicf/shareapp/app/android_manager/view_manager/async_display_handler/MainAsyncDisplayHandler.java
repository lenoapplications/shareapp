package com.example.cacicf.shareapp.app.android_manager.view_manager.async_display_handler;

import android.app.FragmentManager;
import android.os.Handler;
import android.util.Log;

import com.example.cacicf.shareapp.app.android_manager.components_manager.ComponentsManager;
import com.example.cacicf.shareapp.models.android_models.abstracts.android_manager.view_manager.activity_view.ActivityView;
import com.example.cacicf.shareapp.models.android_models.abstracts.android_manager.view_manager.fragment_view.dialog_fragment_view.DialogFragmentView;
import com.example.cacicf.shareapp.models.android_models.abstracts.android_manager.view_manager.fragment_view.fragment_v4_view.FragmentView;
import com.example.cacicf.shareapp.models.android_models.interfaces.android_manager.view_manager.view_setup_model.main_async_display_handler.HandlerRunMethod;
import com.example.cacicf.shareapp.models.helper_models.StaticClass;


public class MainAsyncDisplayHandler extends StaticClass {
    private static MainAsyncDisplayHandler mainAsyncDisplayHandler;
    private ActivityView currentlyActiveActivity;
    private DialogFragmentView currentlyActiveDialogFragment;
    private FragmentView fragmentView;

    private final AsyncLoadingDisplayHandler asyncLoadingDisplayHandler;

    private MainAsyncDisplayHandler(){
        asyncLoadingDisplayHandler = new AsyncLoadingDisplayHandler();

    }
    public static MainAsyncDisplayHandler getMainAsyncDisplayHandler() {
        if (mainAsyncDisplayHandler == null){
            mainAsyncDisplayHandler = new MainAsyncDisplayHandler();
        }
        return mainAsyncDisplayHandler;
    }
    public static void destroyMainAsyncDisplayHandler(){
        mainAsyncDisplayHandler.destroy();
    }


    public void runHandler(final HandlerRunMethod handlerRunMethod){
        Handler handler = new Handler(currentlyActiveActivity.getMainLooper());

        handler.post(new Runnable() {
            @Override
            public void run() {
                handlerRunMethod.run();
            }
        });
    }

    public void activateBasicLoading(){
        Handler handler = new Handler(currentlyActiveActivity.getMainLooper());

        handler.post(new Runnable() {
            @Override
            public void run() {
                asyncLoadingDisplayHandler.activateBasicLoadingDialogFragment(currentlyActiveActivity.getFragmentManager());
            }
        });
    }
    public void deactivateBasicLoading(){
        Handler handler = new Handler(currentlyActiveActivity.getMainLooper());

        handler.post(new Runnable() {
            @Override
            public void run() {
                asyncLoadingDisplayHandler.deactivateBasicLoadingDialogFragment();
            }
        });
    }



    public void setCurrentlyActiveActivity(ActivityView currentlyActiveActivity) {
        this.currentlyActiveActivity = currentlyActiveActivity;
    }

    public void setDialogFragmentView(DialogFragmentView dialogFragmentView) {
        this.currentlyActiveDialogFragment = dialogFragmentView;
    }
    public void setCurrentlyActiveFragmentView(FragmentView fragmentView){
        this.fragmentView = fragmentView;
    }

    public DialogFragmentView getCurrentlyActiveDialogFragment() {
        return currentlyActiveDialogFragment;
    }

    public ActivityView getCurrentlyActiveActivity() {
        return currentlyActiveActivity;
    }

    public FragmentView getCurrentlyActiveFragmentView() {
        return fragmentView;
    }

    public boolean testPurpose(){
        return asyncLoadingDisplayHandler.testPurposeCheckLoading();
    }

    @Override
    public void destroy() {
        mainAsyncDisplayHandler = null;
    }
}
