package com.example.cacicf.shareapp.app.android_manager.view_manager.view_size_handler.font_size_handler;

import android.util.TypedValue;
import android.widget.TextView;

import com.example.cacicf.shareapp.app.android_manager.view_manager.view_system_information_holder.ViewSystemInformationHolder;

public class FontSizeHandler {

    private final double DEFUALT_FONT_PERCATNAGE = 0.0359;

    public FontSizeHandler(){
    }


    public void setFontSizeOfWidget(TextView view){
        view.setTextSize(TypedValue.COMPLEX_UNIT_PX,calculateFontSize() );
    }
    public void setFontSizeOfWidget(TextView view,int parentWidth){
        view.setTextSize(TypedValue.COMPLEX_UNIT_PX ,calculateFontSize(parentWidth) );
    }
    public void setFontSizeOfWidgetCustomPercantage(TextView view, double customPercantage){
        view.setTextSize(TypedValue.COMPLEX_UNIT_PX, calculateFontSizeCustomPercantage(customPercantage));
    }
    public void setFontSizeOfWidgetCustomPercantage(TextView view, double customPercantage, int parentWidth){
        view.setTextSize(TypedValue.COMPLEX_UNIT_PX, calculateFontSizeCustomPercantage(customPercantage, parentWidth));
    }


    private int calculateFontSize(){
        return (int) (ViewSystemInformationHolder.getViewSystemInformationHolder().SCREEN_WIDTH * DEFUALT_FONT_PERCATNAGE);
    }
    private int calculateFontSize(int parentWidth){
        return (int) (parentWidth * DEFUALT_FONT_PERCATNAGE);
    }
    private int calculateFontSizeCustomPercantage(double perctanage){
        return (int) (ViewSystemInformationHolder.getViewSystemInformationHolder().SCREEN_WIDTH * perctanage);
    }
    private int calculateFontSizeCustomPercantage(double perctanage,int parentWidth){
        return (int) (parentWidth * perctanage);
    }
}
