package com.example.cacicf.shareapp.app.android_manager.view_manager.view_size_handler.widget_size_handler;


import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;

import com.example.cacicf.shareapp.app.android_manager.view_manager.view_system_information_holder.ViewSystemInformationHolder;

public class WidgetSizeHandler {


    public WidgetSizeHandler(){

    }

    public void setWidthAndHeightOfWidgetLinearLayout(View view,double widthPercantage, double heightPercantage){
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) view.getLayoutParams();
        double[] widthAndHeight = calculateWindowSize(widthPercantage,heightPercantage);
        layoutParams.width = (int) widthAndHeight[0];
        layoutParams.height = (int)widthAndHeight[1];
        view.setLayoutParams(layoutParams);
    }
    public void setWidthAndHeightOfWidgetRecyclerLayout(View view, double heightPercantage){
        RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
        double[] widthAndHeight = calculateWindowSize(1,heightPercantage);
        layoutParams.width = RecyclerView.LayoutParams.MATCH_PARENT;
        layoutParams.height = (int) widthAndHeight[1];
        view.setLayoutParams(layoutParams);
    }


    private double[] calculateWindowSize(double widthPercantage,double heightPercantage){
        int screenWidth = ViewSystemInformationHolder.getViewSystemInformationHolder().SCREEN_WIDTH;
        int screenHeight = ViewSystemInformationHolder.getViewSystemInformationHolder().SCREEN_HEIGHT;
        return new double[]{screenWidth * widthPercantage , screenHeight * heightPercantage};
    }


}
