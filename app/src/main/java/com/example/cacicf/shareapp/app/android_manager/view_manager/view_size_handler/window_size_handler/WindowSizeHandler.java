package com.example.cacicf.shareapp.app.android_manager.view_manager.view_size_handler.window_size_handler;

import android.view.Window;

import com.example.cacicf.shareapp.app.android_manager.view_manager.view_system_information_holder.ViewSystemInformationHolder;

public class WindowSizeHandler {

    public WindowSizeHandler(){

    }


    public void setWidthAndHeightOfWindow(Window window,double widthPercantage,double heightPercantage){
        double[] widthAndHeight = calculateWindowSize(widthPercantage,heightPercantage);
        window.setLayout((int)widthAndHeight[0],(int)widthAndHeight[1]);
    }


    private double[] calculateWindowSize(double widthPercantage,double heightPercantage){
        int screenWidth = ViewSystemInformationHolder.getViewSystemInformationHolder().SCREEN_WIDTH;
        int screenHeight = ViewSystemInformationHolder.getViewSystemInformationHolder().SCREEN_HEIGHT;
        return new double[]{screenWidth * widthPercantage , screenHeight * heightPercantage};
    }

}
