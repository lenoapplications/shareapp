package com.example.cacicf.shareapp.app.android_manager.view_manager.view_system_information_holder;

import android.content.res.Resources;
import android.util.DisplayMetrics;

public class ViewSystemInformationHolder {
    private static ViewSystemInformationHolder viewSystemInformationHolder;

    public final int SCREEN_WIDTH;
    public final int SCREEN_HEIGHT;

    private ViewSystemInformationHolder(){
        DisplayMetrics displayMetrics = Resources.getSystem().getDisplayMetrics();
        SCREEN_WIDTH = displayMetrics.widthPixels;
        SCREEN_HEIGHT = displayMetrics.heightPixels;
    }

    public static ViewSystemInformationHolder getViewSystemInformationHolder(){
        if (viewSystemInformationHolder == null){
            viewSystemInformationHolder = new ViewSystemInformationHolder();
        }
        return viewSystemInformationHolder;
    }
}
