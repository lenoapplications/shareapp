package com.example.cacicf.shareapp.app.service_layer.notification_classes.connection_notifications;


import android.util.Log;

import com.example.cacicf.shareapp.app.android_manager.view_manager.async_display_handler.MainAsyncDisplayHandler;

import java.io.IOException;
import async_communicator.AsyncCommunicator;
import protocol.statics.class_fixed_messages.ConnectionFixedMessages;
import protocol.statics.class_notification_protocol.ConnectionNotificationProtocol;
import socket_installer.SI_behavior.abstractClasses.notification.data_trade.DataTrade;
import socket_installer.SI_behavior.abstractClasses.notification.notification_state_exceptions.NotificationerStatesBundle;
import socket_installer.SI_behavior.abstractClasses.sockets.socket.client.ClientSocket;
import socket_installer.SI_behavior.abstractClasses.sockets.socket_managers.error_manager.exceptions.SocketExceptions;
import socket_installer.SI_behavior.annotations.user_implementation.methods_implementation.class_annotation.class_identifier.ClassIdentifier;
import socket_installer.SI_behavior.annotations.user_implementation.methods_implementation.methods_annotation.method_identifier.MethodIdentifier;


@ClassIdentifier(identification = ConnectionNotificationProtocol.CLASS_IDENT)
public class ConnectionNotifications extends DataTrade {
    public static final boolean AUTOMATED_TEST = true;

    @MethodIdentifier(identification = ConnectionNotificationProtocol.CHECK_IF_SERVER_ONLINE_RESPONSE)
    public void checkIfServerOnlineResponse(String notification,NotificationerStatesBundle notificationerStatesBundle)throws IOException,SocketExceptions{
        AsyncCommunicator asyncCommunicator = AsyncCommunicator.getAsyncCommunicator();

        String status;
        if (notification.equals(ConnectionFixedMessages.SERVER_ONLINE)){
            status = "ServerOnline";
        }else{
            status = "ServerOffline";
        }
        asyncCommunicator.addResponseToCurrentThread(status);
        //TEST ENVIRONMENT
        notifyThreadFinished(asyncCommunicator);

    }

    @MethodIdentifier(identification = ConnectionNotificationProtocol.CONNECT_TO_SERVER_RESPONSE)
    public void enterToServerResponse(String notificaiton,NotificationerStatesBundle notificationerStatesBundle){
        AsyncCommunicator asyncCommunicator = AsyncCommunicator.getAsyncCommunicator();

        boolean status;
        if(notificaiton.equals(ConnectionFixedMessages.CONNECTION_APPROVED)){
            status = true;
        }else{
            status =false;
        }
        asyncCommunicator.addResponseToCurrentThread(status);
    }

    @Override
    public boolean exceptionHandler(ClientSocket clientSocket, NotificationerStatesBundle notificationerStatesBundle) {
        return false;
    }

    //TEST ENVIRONMENT
    public static final long ID_THREAD_FINISHED = 1;

    private void notifyThreadFinished(AsyncCommunicator asyncCommunicator){
        if (AUTOMATED_TEST){
            asyncCommunicator.initNewThread(ID_THREAD_FINISHED);
            asyncCommunicator.addResponseToFinishedThread(ID_THREAD_FINISHED,true);
            asyncCommunicator.threadFinished(ID_THREAD_FINISHED);
        }
    }
}