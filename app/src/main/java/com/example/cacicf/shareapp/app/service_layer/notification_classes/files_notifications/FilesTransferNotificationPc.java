package com.example.cacicf.shareapp.app.service_layer.notification_classes.files_notifications;

import android.util.Log;

import async_communicator.AsyncCommunicator;
import protocol.statics.class_notification_protocol.FileTransferNotificationProtocol;
import socket_installer.SI_behavior.abstractClasses.notification.data_trade.DataTrade;
import socket_installer.SI_behavior.abstractClasses.notification.notification_state_exceptions.NotificationerStatesBundle;
import socket_installer.SI_behavior.abstractClasses.sockets.socket.client.ClientSocket;
import socket_installer.SI_behavior.annotations.user_implementation.methods_implementation.class_annotation.class_identifier.ClassIdentifier;
import socket_installer.SI_behavior.annotations.user_implementation.methods_implementation.methods_annotation.method_identifier.MethodIdentifier;
import socket_installer.SI_parts.protocol.enum_protocols.data_protocol.DataProtocol;

@ClassIdentifier(identification = FileTransferNotificationProtocol.CLASS_IDENT_PC)
public class FilesTransferNotificationPc extends DataTrade {


    @MethodIdentifier(identification = FileTransferNotificationProtocol.CONNECT_TO_PC_STORAGE_RESPONSE)
    public void connectToPcStorage(String rootStorage,NotificationerStatesBundle notificationerStatesBundle){
        AsyncCommunicator asyncCommunicator = AsyncCommunicator.getAsyncCommunicator();
        asyncCommunicator.addResponseToCurrentThread(rootStorage);
    }

    @MethodIdentifier(identification = FileTransferNotificationProtocol.PC_STORAGE_OPEN_FOLDER_RESPONSE)
    public void openFolder(String newFilesAndFolders,NotificationerStatesBundle notificationerStatesBundle){
        AsyncCommunicator asyncCommunicator = AsyncCommunicator.getAsyncCommunicator();
        asyncCommunicator.addResponseToCurrentThread(newFilesAndFolders);
    }

    @MethodIdentifier(identification = FileTransferNotificationProtocol.PC_STORAGE_CLOSE_FOLDER_RESPONSE)
    public void closeFolder(String newFilesAndFolders,NotificationerStatesBundle notificationerStatesBundle){
        AsyncCommunicator asyncCommunicator = AsyncCommunicator.getAsyncCommunicator();
        asyncCommunicator.addResponseToCurrentThread(newFilesAndFolders);
    }

    @Override
    public boolean exceptionHandler(ClientSocket clientSocket, NotificationerStatesBundle notificationerStatesBundle) {
        return false;
    }
}
