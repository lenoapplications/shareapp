package com.example.cacicf.shareapp.app.service_layer.notification_classes.user_notifications;



import socket_installer.SI_behavior.abstractClasses.notification.data_trade.DataTrade;
import socket_installer.SI_behavior.abstractClasses.notification.notification_state_exceptions.NotificationerStatesBundle;
import socket_installer.SI_behavior.abstractClasses.sockets.socket.client.ClientSocket;
import socket_installer.SI_behavior.annotations.user_implementation.methods_implementation.class_annotation.class_identifier.ClassIdentifier;
import socket_installer.SI_behavior.annotations.user_implementation.methods_implementation.methods_annotation.method_identifier.MethodIdentifier;
import socket_installer.SI_parts.protocol.enum_protocols.data_protocol.DataProtocol;

@ClassIdentifier(identification = "userIdent")
public class UserNotificationMethods extends DataTrade {
    private static final String IDENT = "userIdent";


    @MethodIdentifier(identification = "userIdent_loginAppResponse")
    public void loginAppResponse(String loginStatus, NotificationerStatesBundle notificationerStatesBundle){

    }

    @Override
    public boolean exceptionHandler(ClientSocket clientSocket, NotificationerStatesBundle notificationerStatesBundle) {
        return false;
    }
}
