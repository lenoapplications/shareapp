package com.example.cacicf.shareapp.app.view_layer.user_interface.activites.home_app_activity;



import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;

import com.example.cacicf.shareapp.R;
import com.example.cacicf.shareapp.app.android_manager.view_manager.view_size_handler.font_size_handler.FontSizeHandler;
import com.example.cacicf.shareapp.app.android_manager.view_manager.view_size_handler.widget_size_handler.WidgetSizeHandler;
import com.example.cacicf.shareapp.app.android_manager.view_manager.view_size_handler.window_size_handler.WindowSizeHandler;
import com.example.cacicf.shareapp.app.view_layer.user_interface.activites.home_app_activity.home_app_events.HomeAppEvents;
import com.example.cacicf.shareapp.models.android_models.abstracts.android_manager.view_manager.activity_view.ActivityView;

public class HomeAppActivity extends ActivityView {
    private DrawerLayout homeDrawerLayout;

    @Override
    public void onCreateActivity() {
        setContentView(R.layout.home_app_activity_phones);
        HomeAppEvents homeAppEvents = new HomeAppEvents(this);
        NavigationView navigationView = setupNavigationView();
        homeAppEvents.initActivityEvents(homeDrawerLayout,navigationView);
    }


    @Override
    public void setupView(FontSizeHandler fontSizeHandler, WidgetSizeHandler widgetSizeHandler, WindowSizeHandler windowSizeHandler) {

    }

    private NavigationView setupNavigationView(){
        homeDrawerLayout = findViewById(R.id.homeApp_activity_drawer_layout);
        NavigationView homeNavigationView = (NavigationView)findViewById(R.id.homeApp_activity_navigation_view);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this,
                                                        homeDrawerLayout,
                                                        R.string.open_nav_drawer,
                                                        R.string.close_nav_drawer);
        homeDrawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        return homeNavigationView;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
