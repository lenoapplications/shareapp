package com.example.cacicf.shareapp.app.view_layer.user_interface.activites.home_app_activity.home_app_events;

import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;

import com.example.cacicf.shareapp.app.view_layer.user_interface.activites.home_app_activity.HomeAppActivity;
import com.example.cacicf.shareapp.app.view_layer.user_interface.activites.home_app_activity.home_app_events.nav_bar_events.NavBarEvents;
import com.example.cacicf.shareapp.models.helper_models.StaticClass;

public class HomeAppEvents {
    private HomeAppActivity homeAppActivity;
    private NavBarEvents navBarEvents;


    public void initActivityEvents(DrawerLayout homeDrawerLayout, NavigationView navigationView){
        navigationView.setNavigationItemSelectedListener(navBarEvents.setNavigationItemSelectedListener(homeDrawerLayout,homeAppActivity));
    }


    public HomeAppEvents(HomeAppActivity homeAppActivity){
        this.homeAppActivity = homeAppActivity;
        navBarEvents = new NavBarEvents();
    }

}
