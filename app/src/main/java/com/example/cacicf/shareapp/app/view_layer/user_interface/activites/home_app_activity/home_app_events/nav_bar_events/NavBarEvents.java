package com.example.cacicf.shareapp.app.view_layer.user_interface.activites.home_app_activity.home_app_events.nav_bar_events;

import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.cacicf.shareapp.R;
import com.example.cacicf.shareapp.app.view_layer.user_interface.activites.home_app_activity.HomeAppActivity;
import com.example.cacicf.shareapp.app.view_layer.user_interface.fragments.home_fragments.share_fragment.ShareFragment;
import com.example.cacicf.shareapp.models.android_models.abstracts.android_manager.view_manager.fragment_view.fragment_v4_view.FragmentView;

public class NavBarEvents {

    public NavigationView.OnNavigationItemSelectedListener setNavigationItemSelectedListener(DrawerLayout homeDrawerLayout, HomeAppActivity homeAppActivity){
        return new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.homeApp_activity_navBar_share:{
                        shareFragmentChoosed(homeAppActivity);
                        break;
                    }
                }
                homeDrawerLayout.closeDrawers();
                return true;
            }
        };
    }

    private void shareFragmentChoosed(HomeAppActivity homeAppActivity){
        ShareFragment shareFragment = FragmentView.createInstance(new Object[]{},ShareFragment.class,null);
        shareFragment.startFragmentTransaction(homeAppActivity.getSupportFragmentManager());
    }
}
