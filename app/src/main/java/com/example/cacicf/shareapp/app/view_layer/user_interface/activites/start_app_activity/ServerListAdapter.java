package com.example.cacicf.shareapp.app.view_layer.user_interface.activites.start_app_activity;


import android.graphics.drawable.DrawableContainer;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.example.cacicf.shareapp.R;
import com.example.cacicf.shareapp.app.android_manager.view_manager.view_size_handler.font_size_handler.FontSizeHandler;
import com.example.cacicf.shareapp.app.android_manager.view_manager.view_size_handler.widget_size_handler.WidgetSizeHandler;
import com.example.cacicf.shareapp.app.android_manager.view_manager.view_size_handler.window_size_handler.WindowSizeHandler;
import com.example.cacicf.shareapp.app.view_layer.user_interface.activites.start_app_activity.start_app_events.server_list_events.ServerListEvents;
import com.example.cacicf.shareapp.models.android_models.abstracts.android_manager.view_manager.adapter_view.recycler_view.RecyclerAdapterView;
import com.example.cacicf.shareapp.models.data_objects_holder.recylcer_view_objects.start_app_server_list.DataServerObjectHolder;

import java.util.ArrayList;
import java.util.Arrays;

public class ServerListAdapter extends RecyclerAdapterView<DataServerObjectHolder,ServerListAdapter.ServerListViewHolder> {
    private final ServerListEvents serverListEvents;
    private final StartAppActivity startAppActivity;

    class ServerListViewHolder extends RecyclerView.ViewHolder{
        public View cardView;
        public TextView serverIdent;
        public TextView serverIp;
        public Button connectToServer;

        public ServerListViewHolder(View itemView) {
            super(itemView);
            cardView = itemView;
            serverIdent = itemView.findViewById(R.id.startApp_activity_recyclerViewTextView_card_serverIdent);
            serverIp = itemView.findViewById(R.id.startApp_activity_recyclerViewTextView_card_serverIp);
            connectToServer = itemView.findViewById(R.id.startApp_activity_recyclerViewButton_card_connect);
        }
    }
    protected ServerListAdapter(StartAppActivity startAppActivity) {
        super(new ArrayList<DataServerObjectHolder>());
        serverListEvents = new ServerListEvents();
        this.startAppActivity = startAppActivity;
    }


    @Override
    public void onBindRecylcerView(RecyclerView.ViewHolder holder, int position) {
        ServerListViewHolder serverListViewHolder = (ServerListViewHolder) holder;
        DataServerObjectHolder dataServerObjectHolder = getItem(position);

        serverListViewHolder.serverIdent.setText(checkServerName(dataServerObjectHolder.getServerName()));
        serverListViewHolder.serverIp.setText(dataServerObjectHolder.getServerIp());
        checkServerStatus(serverListViewHolder.cardView,dataServerObjectHolder.getServerStatus());
        checkIfServerIsMarked(serverListViewHolder.cardView,dataServerObjectHolder.isDeleteServer());

        serverListEvents.initServerListEvents(startAppActivity,serverListViewHolder.cardView,this,dataServerObjectHolder);
    }

    @Override
    public int onCreateCardView() {
        return R.layout.start_app_card_server_list;
    }

    @Override
    public RecyclerView.ViewHolder getViewHolder() {
        return new ServerListViewHolder(cardViewTemplate);
    }

    @Override
    public void setupView(FontSizeHandler fontSizeHandler, WidgetSizeHandler widgetSizeHandler, WindowSizeHandler windowSizeHandler) {
        TextView serverIdent = (TextView) cardViewTemplate.findViewById(R.id.startApp_activity_recyclerViewTextView_card_serverIdent);
        TextView serverIp = (TextView) cardViewTemplate.findViewById(R.id.startApp_activity_recyclerViewTextView_card_serverIp);
        Button connectToServer = (Button) cardViewTemplate.findViewById(R.id.startApp_activity_recyclerViewButton_card_connect);

        widgetSizeHandler.setWidthAndHeightOfWidgetRecyclerLayout(cardViewTemplate,0.08);
        widgetSizeHandler.setWidthAndHeightOfWidgetLinearLayout(connectToServer,0.20,0.05);

        fontSizeHandler.setFontSizeOfWidgetCustomPercantage(serverIdent,0.043);
        fontSizeHandler.setFontSizeOfWidgetCustomPercantage(serverIp,0.030);
        fontSizeHandler.setFontSizeOfWidgetCustomPercantage(connectToServer,0.023);
    }

    private String checkServerName(String serverName){
        if (serverName.length() > 12){
            return serverName.substring(0,10) + "...";
        }
        return serverName;
    }


    private void checkServerStatus(View cardView,boolean status){
        StateListDrawable drawable = (StateListDrawable)cardView.findViewById(R.id.startApp_activity_recyclerViewButton_card_connect).getBackground();
        DrawableContainer.DrawableContainerState dcs = (DrawableContainer.DrawableContainerState)drawable.getConstantState();
        GradientDrawable drawableBorder = (GradientDrawable) dcs.getChildren()[0];

        if (status){
            drawableBorder.setStroke(4,ContextCompat.getColor(cardView.getContext(),R.color.startAppActivity_serverOnlineColor));
        }else{
            drawableBorder.setStroke(4,ContextCompat.getColor(cardView.getContext(),R.color.startAppActivity_serverOfflineColor));
        }
    }
    private void checkIfServerIsMarked(View cardView,boolean deleteStatus){
        View mainLayout = cardView.findViewById(R.id.startApp_activity_recyclerView_card_mainLayout);
        if (deleteStatus){
            mainLayout.setBackground(ContextCompat.getDrawable(cardView.getContext(),R.drawable.recantgle_top_bottom_border));
        }else{
            mainLayout.setBackground(null);
        }
    }
}
