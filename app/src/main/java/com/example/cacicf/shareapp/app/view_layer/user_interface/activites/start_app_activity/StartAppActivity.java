package com.example.cacicf.shareapp.app.view_layer.user_interface.activites.start_app_activity;


import android.view.View;
import android.widget.Button;
import com.example.cacicf.shareapp.R;
import com.example.cacicf.shareapp.app.android_manager.components_manager.ComponentsManager;
import com.example.cacicf.shareapp.app.android_manager.view_manager.ViewManager;
import com.example.cacicf.shareapp.app.android_manager.view_manager.async_display_handler.AsyncLoadingDisplayHandler;
import com.example.cacicf.shareapp.app.android_manager.view_manager.async_display_handler.MainAsyncDisplayHandler;
import com.example.cacicf.shareapp.app.android_manager.view_manager.view_size_handler.font_size_handler.FontSizeHandler;
import com.example.cacicf.shareapp.app.android_manager.view_manager.view_size_handler.widget_size_handler.WidgetSizeHandler;
import com.example.cacicf.shareapp.app.android_manager.view_manager.view_size_handler.window_size_handler.WindowSizeHandler;
import com.example.cacicf.shareapp.app.view_layer.user_interface.activites.start_app_activity.start_app_events.StartAppEvents;
import com.example.cacicf.shareapp.models.android_models.abstracts.android_manager.view_manager.activity_view.ActivityView;
import com.example.cacicf.shareapp.models.android_models.abstracts.android_manager.view_manager.adapter_view.recycler_view.RecyclerAdapterView;


public class StartAppActivity extends ActivityView {

    public StartAppActivity(){
        super();
    }


    @Override
    public void onCreateActivity() {
        setContentView(R.layout.start_app_activity_phones);
        StartAppEvents startAppEvents = new StartAppEvents();
        View view = findViewById(R.id.startApp_activity_mainLayout_serverList);
        ServerListAdapter serverListAdapter = new ServerListAdapter(this);


        RecyclerAdapterView.createRecyclerViewAdapter(this,view, serverListAdapter,R.id.startApp_activity_recyclerView_serverList);
        startAppEvents.initActivityEvents(this,serverListAdapter);
    }


    @Override
    public void setupView(FontSizeHandler fontSizeHandler, WidgetSizeHandler widgetSizeHandler, WindowSizeHandler windowSizeHandler) {
        fontSizeHandler.setFontSizeOfWidget((Button) findViewById(R.id.startApp_activity_button_addServer));
        fontSizeHandler.setFontSizeOfWidgetCustomPercantage((Button)findViewById(R.id.startApp_activity_button_deleteServers),0.029);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ComponentsManager.destroyComponentManager();
        ViewManager.destroyViewManager();
        MainAsyncDisplayHandler.destroyMainAsyncDisplayHandler();

    }
}
