package com.example.cacicf.shareapp.app.view_layer.user_interface.activites.start_app_activity.start_app_events;


import android.util.Log;
import android.view.View;
import android.widget.Button;
import com.example.cacicf.shareapp.R;
import com.example.cacicf.shareapp.app.view_layer.user_interface.activites.start_app_activity.ServerListAdapter;
import com.example.cacicf.shareapp.app.view_layer.user_interface.activites.start_app_activity.StartAppActivity;
import com.example.cacicf.shareapp.app.view_layer.user_interface.fragments.dialog_fragments.add_server_dialog_fragment.AddServerDialogFragment;
import com.example.cacicf.shareapp.models.android_models.abstracts.android_manager.view_manager.fragment_view.dialog_fragment_view.DialogFragmentView;
import com.example.cacicf.shareapp.models.data_objects_holder.recylcer_view_objects.start_app_server_list.DataServerObjectHolder;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class StartAppEvents {

    public void initActivityEvents(StartAppActivity startAppActivity, ServerListAdapter serverListAdapter){
        addServerButtonOnClickListener(startAppActivity,serverListAdapter);
        deleteServersButtonOnClickListener(startAppActivity,serverListAdapter);
    }

    private void addServerButtonOnClickListener(final StartAppActivity startAppActivity ,final ServerListAdapter serverListAdapter){
        final Button addServerButton = startAppActivity.findViewById(R.id.startApp_activity_button_addServer);

        addServerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Object[] bundleObject = new Object[]{serverListAdapter};
                AddServerDialogFragment addServerDialogFragment = DialogFragmentView.createInstance(bundleObject,AddServerDialogFragment.class,null);
                addServerDialogFragment.show(startAppActivity.getFragmentManager());
            }
        });
    }
    private void deleteServersButtonOnClickListener(final StartAppActivity startAppActivity,ServerListAdapter serverListAdapter){
        final Button deleteServerButton = startAppActivity.findViewById(R.id.startApp_activity_button_deleteServers);

        deleteServerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<DataServerObjectHolder> dataServerObjectHolders = serverListAdapter.getCompleteList();

                for (int i = 0; i < dataServerObjectHolders.size(); ++i){
                    if (dataServerObjectHolders.get(i).isDeleteServer()){
                        serverListAdapter.removeItem(i);
                        --i;
                    }
                }
            }
        });

    }
}
