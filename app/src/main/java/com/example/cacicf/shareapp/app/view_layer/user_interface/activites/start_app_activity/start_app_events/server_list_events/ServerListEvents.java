package com.example.cacicf.shareapp.app.view_layer.user_interface.activites.start_app_activity.start_app_events.server_list_events;


import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import com.example.cacicf.shareapp.R;
import com.example.cacicf.shareapp.app.android_manager.components_manager.ComponentsManager;
import com.example.cacicf.shareapp.app.view_layer.user_interface.activites.home_app_activity.HomeAppActivity;
import com.example.cacicf.shareapp.app.view_layer.user_interface.activites.start_app_activity.ServerListAdapter;
import com.example.cacicf.shareapp.app.view_layer.user_interface.activites.start_app_activity.StartAppActivity;
import com.example.cacicf.shareapp.components.components_configuration.socket_installer_config.client.Client;
import com.example.cacicf.shareapp.models.data_objects_holder.recylcer_view_objects.start_app_server_list.DataServerObjectHolder;
import async_communicator.AsyncCommunicator;
import async_communicator.thread_id_holder.ThreadIdHolder;
import protocol.statics.class_fixed_messages.ConnectionFixedMessages;
import protocol.statics.class_notification_protocol.ConnectionNotificationProtocol;


public class ServerListEvents {

    public void initServerListEvents(StartAppActivity startAppActivity, View cardView, ServerListAdapter serverListAdapter, DataServerObjectHolder dataServerObjectHolder){
        clickOnCardViewListener(cardView,serverListAdapter,dataServerObjectHolder);
        clickOnConnectButtonListener(startAppActivity,cardView,dataServerObjectHolder.getServerStatus());
    }

    private void clickOnCardViewListener(View cardView, ServerListAdapter serverListAdapter, DataServerObjectHolder dataServerObjectHolder){
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dataServerObjectHolder.isDeleteServer()){
                    dataServerObjectHolder.setDeleteServer(false);
                }else{
                    dataServerObjectHolder.setDeleteServer(true);
                }
                serverListAdapter.notifyDataSetChanged();
            }
        });
    }

    private void clickOnConnectButtonListener(StartAppActivity startAppActivity,View cardView,boolean serverStatus){
        Button connect = (Button) cardView.findViewById(R.id.startApp_activity_recyclerViewButton_card_connect);

        connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (serverStatus){
                    ComponentsManager componentsManager = ComponentsManager.getComponentsManager();
                    Client client = componentsManager.getClient();
                    ThreadIdHolder idHolder = client.sendMessageToServer(ConnectionNotificationProtocol.CLASS_IDENT,
                                                    ConnectionNotificationProtocol.CONNECT_TO_SERVER_REQUEST,
                                                    ConnectionFixedMessages.CONNECT_REQUEST);
                    checkConnectionToServerStatus(startAppActivity,idHolder.getThreadId());
                }
            }
        });
    }
    private void checkConnectionToServerStatus(StartAppActivity startAppActivity,long threadId){
        AsyncCommunicator asyncCommunicator = AsyncCommunicator.getAsyncCommunicator();

        boolean connectedStatus = asyncCommunicator.waitThreadForResponse(threadId);
        if(connectedStatus){
            Intent intent = new Intent(startAppActivity, HomeAppActivity.class);
            startAppActivity.startActivity(intent);
        }else{
        }
    }

}
