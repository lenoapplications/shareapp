package com.example.cacicf.shareapp.app.view_layer.user_interface.fragments.dialog_fragments.add_server_dialog_fragment;


import android.app.Dialog;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.cacicf.shareapp.R;
import com.example.cacicf.shareapp.app.android_manager.view_manager.view_size_handler.font_size_handler.FontSizeHandler;
import com.example.cacicf.shareapp.app.android_manager.view_manager.view_size_handler.widget_size_handler.WidgetSizeHandler;
import com.example.cacicf.shareapp.app.android_manager.view_manager.view_size_handler.window_size_handler.WindowSizeHandler;
import com.example.cacicf.shareapp.app.view_layer.user_interface.activites.start_app_activity.ServerListAdapter;
import com.example.cacicf.shareapp.app.view_layer.user_interface.fragments.dialog_fragments.add_server_dialog_fragment.add_server_events.AddServerEvents;
import com.example.cacicf.shareapp.models.android_models.abstracts.android_manager.view_manager.fragment_view.dialog_fragment_view.DialogFragmentView;

public class AddServerDialogFragment extends DialogFragmentView {


    public AddServerDialogFragment() {
        super("ADD_SERVER_DIALOG_FRAGMENT");
    }


    @Override
    public void onCreateFragment() {
        mainAsyncDisplayHandler.setDialogFragmentView(this);
        ServerListAdapter serverListAdapter = (ServerListAdapter) getArguments().getSerializable("ServerListAdapter");
        AddServerEvents addServerEvent = new AddServerEvents();
        addServerEvent.initFragmentEvents(this,serverListAdapter);
    }


    @Override
    public int onCreateFragmentView() {
        return R.layout.start_app_add_server_fragment_phones;
    }

    @Override
    public void onDismissFragmentView() {
        mainAsyncDisplayHandler.setDialogFragmentView(null);
    }

    @Override
    public void setupView(FontSizeHandler fontSizeHandler, WidgetSizeHandler widgetSizeHandler, WindowSizeHandler windowSizeHandler) {
        double headlinePerc = 0.05;
        double editAndTextPerc = 0.0280;
        double buttonPerc = 0.0280;
        Dialog addServerView = getDialog();
        windowSizeHandler.setWidthAndHeightOfWindow(addServerView.getWindow(),0.9,0.5);
        addServerView.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        TextView headline = addServerView.findViewById(R.id.startApp_dialogFragmentAddServer_textView_headline);
        TextView ipAddress = addServerView.findViewById(R.id.startApp_dialogFragmentAddServer_textView_ipAddress);
        EditText editTextIpAddress = addServerView.findViewById(R.id.startApp_dialogFragmentAddServer_editText_ipAddress);
        TextView serverName = addServerView.findViewById(R.id.startApp_dialogFragmentAddServer_textView_serverName);
        EditText editTextServerName = addServerView.findViewById(R.id.startApp_dialogFragmentAddServer_editText_serverName);
        Button saveIpAddress = addServerView.findViewById(R.id.startApp_dialogFragmentAddServer_button_saveIpAdress);


        fontSizeHandler.setFontSizeOfWidgetCustomPercantage(headline,headlinePerc);
        fontSizeHandler.setFontSizeOfWidgetCustomPercantage(ipAddress,editAndTextPerc);
        fontSizeHandler.setFontSizeOfWidgetCustomPercantage(editTextIpAddress,editAndTextPerc);
        fontSizeHandler.setFontSizeOfWidgetCustomPercantage(serverName,editAndTextPerc);
        fontSizeHandler.setFontSizeOfWidgetCustomPercantage(editTextServerName,editAndTextPerc);
        fontSizeHandler.setFontSizeOfWidgetCustomPercantage(saveIpAddress,buttonPerc);
    }

}
