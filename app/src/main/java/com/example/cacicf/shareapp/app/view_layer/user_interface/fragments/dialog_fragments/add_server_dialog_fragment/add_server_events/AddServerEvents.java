package com.example.cacicf.shareapp.app.view_layer.user_interface.fragments.dialog_fragments.add_server_dialog_fragment.add_server_events;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.cacicf.shareapp.R;
import com.example.cacicf.shareapp.app.android_manager.components_manager.ComponentsManager;
import com.example.cacicf.shareapp.app.android_manager.view_manager.async_display_handler.AsyncLoadingDisplayHandler;
import com.example.cacicf.shareapp.app.android_manager.view_manager.async_display_handler.MainAsyncDisplayHandler;
import com.example.cacicf.shareapp.app.view_layer.user_interface.activites.start_app_activity.ServerListAdapter;
import com.example.cacicf.shareapp.app.view_layer.user_interface.fragments.dialog_fragments.add_server_dialog_fragment.AddServerDialogFragment;
import com.example.cacicf.shareapp.components.components_configuration.socket_installer_config.client.Client;
import com.example.cacicf.shareapp.components.components_implementation.thread_watcher_implementation.thread_caller.AndroidThreadCaller;
import com.example.cacicf.shareapp.components.components_implementation.thread_watcher_implementation.thread_classes.method_processor.MethodProcessor;
import com.example.cacicf.shareapp.models.android_models.abstracts.android_manager.view_manager.fragment_view.dialog_fragment_view.DialogFragmentView;
import com.example.cacicf.shareapp.models.android_models.interfaces.android_manager.view_manager.view_setup_model.main_async_display_handler.HandlerRunMethod;
import com.example.cacicf.shareapp.models.components_models.thread_watcher.thread_classes.method_processor.MethodProcessorModel;
import com.example.cacicf.shareapp.models.data_objects_holder.recylcer_view_objects.start_app_server_list.DataServerObjectHolder;

import async_communicator.AsyncCommunicator;
import async_communicator.thread_id_holder.ThreadIdHolder;

public class AddServerEvents {


    public void initFragmentEvents(AddServerDialogFragment dialogFragmentView, ServerListAdapter serverListAdapter){
        saveIpAddressButtonOnClick(dialogFragmentView,serverListAdapter);
    }

    private void saveIpAddressButtonOnClick(final AddServerDialogFragment dialogFragmentView, final ServerListAdapter serverListAdapter){
        Button saveIpAddress = dialogFragmentView.getView().findViewById(R.id.startApp_dialogFragmentAddServer_button_saveIpAdress);

        saveIpAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Client client = ComponentsManager.getComponentsManager().getClient();

                if (checkIfFieldsAreNotEmpty(dialogFragmentView)){
                    String stringIpAddress =((EditText) dialogFragmentView.getView().findViewById(R.id.startApp_dialogFragmentAddServer_editText_ipAddress)).getText().toString();
                    configureSocket(dialogFragmentView,serverListAdapter,client,stringIpAddress,3000,10);
                }

            }
        });
    }

    private boolean checkIfFieldsAreNotEmpty(DialogFragmentView dialogFragmentView){
        EditText serverIpAddress =((EditText) dialogFragmentView.getView().findViewById(R.id.startApp_dialogFragmentAddServer_editText_ipAddress));
        EditText serverName =((EditText) dialogFragmentView.getView().findViewById(R.id.startApp_dialogFragmentAddServer_editText_serverName));
        int ipLength = serverIpAddress.getText().toString().length();
        int nameLength = serverName.getText().toString().length();
        String errorIp = (ipLength == 0)? dialogFragmentView.getString(R.string.startAppActivity_fragmentAddServer_errors_fieldMustNotBeEmpty) : null;
        String errorName = (nameLength == 0)? dialogFragmentView.getString(R.string.startAppActivity_fragmentAddServer_errors_fieldMustNotBeEmpty) : null;

        serverIpAddress.setError(errorIp);
        serverName.setError(errorName);

        return ipLength != 0 && nameLength != 0;
    }


    private void configureSocket(DialogFragmentView dialogFragmentView,ServerListAdapter serverListAdapter,Client client,String host,int port,int timeout){
        ThreadIdHolder idHolder = client.configureSocket(host,port,timeout);
        AndroidThreadCaller.callMethodProcessor(checkIfServerIsOnline(dialogFragmentView,serverListAdapter,host,idHolder));
        MainAsyncDisplayHandler.getMainAsyncDisplayHandler().activateBasicLoading();
    }

    private MethodProcessorModel checkIfServerIsOnline(DialogFragmentView dialogFragmentView, ServerListAdapter serverListAdapter, String host, ThreadIdHolder idHolder){
        return new MethodProcessorModel() {
            @Override
            public void process(long threadId) {
                AsyncCommunicator asyncCommunicator = AsyncCommunicator.getAsyncCommunicator();
                String serverStatus = asyncCommunicator.waitThreadForResponse(idHolder.getThreadId());
                MainAsyncDisplayHandler.getMainAsyncDisplayHandler().deactivateBasicLoading();
                switch (serverStatus){
                    case "ServerOnline": {
                        addServerToServerList(dialogFragmentView, serverListAdapter, host);
                        break;
                    }
                    case "UnknownHostException":{
                        displayUnknownHostException(dialogFragmentView);
                        break;
                    }
                    case "SocketTimeoutException":{
                        displaySocketTimeoutException(dialogFragmentView,serverListAdapter,host);
                        break;
                    }
                    default:
                        displayDefaultException(dialogFragmentView,host);
                }
            }
        };
    }
    private void addServerToServerList(DialogFragmentView dialogFragmentView,ServerListAdapter serverListAdapter,String host){
        MainAsyncDisplayHandler.getMainAsyncDisplayHandler().runHandler(new HandlerRunMethod() {
            @Override
            public void run() {
                String serverName = ((EditText)dialogFragmentView.getView().findViewById(R.id.startApp_dialogFragmentAddServer_editText_serverName)).getText().toString();
                serverListAdapter.addItem(new DataServerObjectHolder(serverName,host,true));
            }
        });
    }


    private void displayUnknownHostException(DialogFragmentView dialogFragmentView){
        MainAsyncDisplayHandler.getMainAsyncDisplayHandler().runHandler(new HandlerRunMethod() {
            @Override
            public void run() {
                String errorMessage = dialogFragmentView.getString(R.string.startAppActivity_fragmentAddServer_errors_unknownhostException);
                EditText serverIpField = (EditText) dialogFragmentView.getView().findViewById(R.id.startApp_dialogFragmentAddServer_editText_ipAddress);
                serverIpField.setError(errorMessage);
            }
        });
    }

    private void displaySocketTimeoutException(DialogFragmentView dialogFragmentView,ServerListAdapter serverListAdapter,String host){
        MainAsyncDisplayHandler.getMainAsyncDisplayHandler().runHandler(new HandlerRunMethod() {
            @Override
            public void run() {
                String serverName = ((EditText) dialogFragmentView.getView().findViewById(R.id.startApp_dialogFragmentAddServer_editText_serverName)).getText().toString();
                serverListAdapter.addItem(new DataServerObjectHolder(serverName,host,false));
            }
        });
    }

    private void displayDefaultException(DialogFragmentView dialogFragmentView,String host){
        MainAsyncDisplayHandler.getMainAsyncDisplayHandler().runHandler(new HandlerRunMethod() {
            @Override
            public void run() {
                String errorMessage = dialogFragmentView.getString(R.string.startAppActivity_fragmentAddServer_errors_default,host);
                Toast.makeText(dialogFragmentView.getActivity(),errorMessage,Toast.LENGTH_LONG).show();
            }
        });
    }
}
