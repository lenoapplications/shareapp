package com.example.cacicf.shareapp.app.view_layer.user_interface.fragments.dialog_fragments.loading_dialog_fragments;

import android.app.Dialog;
import android.app.FragmentManager;
import android.os.Bundle;
import android.view.Window;


import com.example.cacicf.shareapp.R;
import com.example.cacicf.shareapp.app.android_manager.view_manager.view_size_handler.font_size_handler.FontSizeHandler;
import com.example.cacicf.shareapp.app.android_manager.view_manager.view_size_handler.widget_size_handler.WidgetSizeHandler;
import com.example.cacicf.shareapp.app.android_manager.view_manager.view_size_handler.window_size_handler.WindowSizeHandler;
import com.example.cacicf.shareapp.models.android_models.abstracts.android_manager.view_manager.fragment_view.dialog_fragment_view.DialogFragmentView;

import async_communicator.AsyncCommunicator;


public class BasicLoadingDialogFragment extends DialogFragmentView {
    //TEST PURPOSE
    public static final long THREAD_LOADING = 3699;
    public static final String DIALOG_DISMISS = "DlgDsmss";

    public BasicLoadingDialogFragment() {
        super("BASIC_LOADING_DIALOG_FRAGMENT");
    }


    @Override
    public void onCreateFragment() {
        setCancelable(false);

        //TEST PURPOSE
        AsyncCommunicator.getAsyncCommunicator().initNewThread(THREAD_LOADING);
    }


    @Override
    public int onCreateFragmentView() {
        return R.layout.general_fragment_basic_loading;
    }

    @Override
    public void onDismissFragmentView() {
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        AsyncCommunicator.getAsyncCommunicator().addFlag(THREAD_LOADING,DIALOG_DISMISS,true);
    }

    @Override
    public void setupView(FontSizeHandler fontSizeHandler, WidgetSizeHandler widgetSizeHandler, WindowSizeHandler windowSizeHandler) {
        Dialog loadingDialog = getDialog();
        windowSizeHandler.setWidthAndHeightOfWindow(loadingDialog.getWindow(),0.8,0.1765);
    }
    public void showLoading(FragmentManager fragmentManager){
        show(fragmentManager);
    }

}
