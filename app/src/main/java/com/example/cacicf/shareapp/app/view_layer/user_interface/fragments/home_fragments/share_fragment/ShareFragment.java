package com.example.cacicf.shareapp.app.view_layer.user_interface.fragments.home_fragments.share_fragment;

import android.widget.Button;

import com.example.cacicf.shareapp.R;
import com.example.cacicf.shareapp.app.android_manager.view_manager.view_size_handler.font_size_handler.FontSizeHandler;
import com.example.cacicf.shareapp.app.android_manager.view_manager.view_size_handler.widget_size_handler.WidgetSizeHandler;
import com.example.cacicf.shareapp.app.android_manager.view_manager.view_size_handler.window_size_handler.WindowSizeHandler;
import com.example.cacicf.shareapp.app.view_layer.user_interface.fragments.home_fragments.share_fragment.share_events.ShareEvents;
import com.example.cacicf.shareapp.models.android_models.abstracts.android_manager.view_manager.fragment_view.fragment_v4_view.FragmentView;

public class ShareFragment extends FragmentView{

    public ShareFragment(){
        super("HOME_APP_SHARE_FRAGMENT", R.id.homeApp_activity_frameLayout);
    }

    @Override
    public void onCreateFragment() {
        mainAsyncDisplayHandler.setCurrentlyActiveFragmentView(this);
        ShareEvents shareEvents = new ShareEvents();
        shareEvents.initFragmentEvents(this);
    }

    @Override
    public int onCreateFragmentView() {
        return R.layout.home_app_activity_fragment_share_phones;
    }

    @Override
    public void onDismissFragmentView() {
        mainAsyncDisplayHandler.setCurrentlyActiveFragmentView(null);
    }

    @Override
    public void setupView(FontSizeHandler fontSizeHandler, WidgetSizeHandler widgetSizeHandler, WindowSizeHandler windowSizeHandler) {
        Button pcButton = (Button)getView().findViewById(R.id.homeApp_activity_shareFragment_head_bar_button_pc);
        Button phoneButton = (Button) getView().findViewById(R.id.homeApp_activity_shareFragment_head_bar_button_phone);

        fontSizeHandler.setFontSizeOfWidget(pcButton);
        fontSizeHandler.setFontSizeOfWidget(phoneButton);

    }
}
