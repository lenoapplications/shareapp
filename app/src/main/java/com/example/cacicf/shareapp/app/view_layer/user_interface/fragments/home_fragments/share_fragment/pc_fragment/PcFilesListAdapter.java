package com.example.cacicf.shareapp.app.view_layer.user_interface.fragments.home_fragments.share_fragment.pc_fragment;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.cacicf.shareapp.R;
import com.example.cacicf.shareapp.app.android_manager.view_manager.view_size_handler.font_size_handler.FontSizeHandler;
import com.example.cacicf.shareapp.app.android_manager.view_manager.view_size_handler.widget_size_handler.WidgetSizeHandler;
import com.example.cacicf.shareapp.app.android_manager.view_manager.view_size_handler.window_size_handler.WindowSizeHandler;
import com.example.cacicf.shareapp.app.view_layer.user_interface.fragments.home_fragments.share_fragment.pc_fragment.pc_fragment_events.pc_files_list_event.PcFilesListEvents;
import com.example.cacicf.shareapp.models.android_models.abstracts.android_manager.view_manager.adapter_view.recycler_view.RecyclerAdapterView;
import com.example.cacicf.shareapp.models.data_objects_holder.recylcer_view_objects.home_app_lists.share_fragment_lists.FileSystemObjectsHolder;

import java.util.ArrayList;

import static protocol.statics.class_fixed_messages.FileTransferFixedMessages.FOLDER_EMPTY;

public class PcFilesListAdapter extends RecyclerAdapterView<FileSystemObjectsHolder,PcFilesListAdapter.FileSystemObjectsViewHolder>{
    private final PcFilesListEvents pcFilesListEvents;

    public PcFilesListAdapter(String root) {
        super(new ArrayList<FileSystemObjectsHolder>());
        parseStringForFilesAndFolders(root);

        pcFilesListEvents = new PcFilesListEvents();
    }
    class FileSystemObjectsViewHolder extends RecyclerView.ViewHolder{
        public View cardView;
        public TextView fileName;
        public ImageView fileFolderImage;

        public FileSystemObjectsViewHolder(View itemView) {
            super(itemView);
            this.cardView = itemView;
            fileName = cardView.findViewById(R.id.homeApp_activity_shareFragment_pcFragment_filesPreview_layout_list_recyclerView_fileFolderFileName);
            fileFolderImage = cardView.findViewById(R.id.homeApp_activity_shareFragment_pcFragment_filesPreview_layout_list_recyclerView_fileFolderImage);
        }
    }

    @Override
    public void onBindRecylcerView(RecyclerView.ViewHolder holder, int position) {
        FileSystemObjectsViewHolder fileSystemObjectsViewHolder = (FileSystemObjectsViewHolder) holder;
        FileSystemObjectsHolder fileSystemObjectsHolder = getItem(position);


        fileSystemObjectsViewHolder.fileName.setText(fileSystemObjectsHolder.getFileName());
        checkIfFileOrFolder(fileSystemObjectsHolder,fileSystemObjectsViewHolder);

        pcFilesListEvents.initPcFilesListEvents(this,fileSystemObjectsViewHolder.cardView,fileSystemObjectsHolder);
    }

    @Override
    public int onCreateCardView() {
        return R.layout.home_app_activity_fragment_share_pc_card_file_list;
    }

    @Override
    public RecyclerView.ViewHolder getViewHolder() {
        return new FileSystemObjectsViewHolder(cardViewTemplate);
    }

    @Override
    public void setupView(FontSizeHandler fontSizeHandler, WidgetSizeHandler widgetSizeHandler, WindowSizeHandler windowSizeHandler) {
        TextView fileName = cardViewTemplate.findViewById(R.id.homeApp_activity_shareFragment_pcFragment_filesPreview_layout_list_recyclerView_fileFolderFileName);

        fontSizeHandler.setFontSizeOfWidgetCustomPercantage(fileName,0.05);

    }

    public void replaceCurrentFilesWithNew(String newFilesAndFolders){
        parseStringForFilesAndFolders(newFilesAndFolders);
    }


    private void parseStringForFilesAndFolders(String unparsedString){
        clearItemList();

        if (!unparsedString.equals(FOLDER_EMPTY)){
            String[] parsedString = unparsedString.split("\n");
            for (int i = 0; i<parsedString.length;++i){
                addItem(new FileSystemObjectsHolder(parsedString[i]));
            }
        }
    }

    private void checkIfFileOrFolder(FileSystemObjectsHolder fileSystemObjectsHolder,FileSystemObjectsViewHolder fileSystemObjectsViewHolder) {
        if (fileSystemObjectsHolder.isDirectory()){
            fileSystemObjectsViewHolder.fileFolderImage.setBackgroundResource(R.drawable.list_folder_img);
        }else{
            fileSystemObjectsViewHolder.fileFolderImage.setBackgroundResource(R.drawable.list_file_picture_img);
        }

    }
}
