package com.example.cacicf.shareapp.app.view_layer.user_interface.fragments.home_fragments.share_fragment.pc_fragment;

import android.view.View;
import android.widget.ImageButton;

import com.example.cacicf.shareapp.R;
import com.example.cacicf.shareapp.app.android_manager.view_manager.view_size_handler.font_size_handler.FontSizeHandler;
import com.example.cacicf.shareapp.app.android_manager.view_manager.view_size_handler.widget_size_handler.WidgetSizeHandler;
import com.example.cacicf.shareapp.app.android_manager.view_manager.view_size_handler.window_size_handler.WindowSizeHandler;
import com.example.cacicf.shareapp.app.view_layer.user_interface.activites.home_app_activity.HomeAppActivity;
import com.example.cacicf.shareapp.app.view_layer.user_interface.fragments.home_fragments.share_fragment.pc_fragment.pc_fragment_events.PcFragmentEvents;
import com.example.cacicf.shareapp.models.android_models.abstracts.android_manager.view_manager.adapter_view.recycler_view.RecyclerAdapterView;
import com.example.cacicf.shareapp.models.android_models.abstracts.android_manager.view_manager.fragment_view.fragment_v4_view.FragmentView;

public class SharePcFragment extends FragmentView {

    public SharePcFragment() {
        super("SHARE_PC_FRAGMENT", R.id.homeApp_activity_shareFragment_frame_layout);
    }

    @Override
    public void onCreateFragment() {
        mainAsyncDisplayHandler.setCurrentlyActiveFragmentView(this);
        inPcFragment();
    }

    @Override
    public int onCreateFragmentView() {
        return R.layout.home_app_activity_fragment_share_fragment_pc_phones;
    }

    @Override
    public void onDismissFragmentView() {
        mainAsyncDisplayHandler.setCurrentlyActiveFragmentView(null);
    }

    @Override
    public void setupView(FontSizeHandler fontSizeHandler, WidgetSizeHandler widgetSizeHandler, WindowSizeHandler windowSizeHandler) {
        View view = getView();

        ImageButton goBackButton = view.findViewById(R.id.homeApp_activity_shareFragment_pcFragment_filesPreview_layout_list_actionBarLayout_imageButton_goBack);

        widgetSizeHandler.setWidthAndHeightOfWidgetLinearLayout(goBackButton,0.12,0.05);

    }


    private void inPcFragment(){
        PcFragmentEvents pcFragmentEvents = new PcFragmentEvents();

        String root  = (String)getArguments().getSerializable("String");
        PcFilesListAdapter pcFilesListAdapter = new PcFilesListAdapter(root);
        RecyclerAdapterView.createRecyclerViewAdapter((HomeAppActivity)getActivity(),getView(),pcFilesListAdapter,R.id.homeApp_activity_shareFragment_pcFragment_filesPreview_layout_list_recyclerView);

        pcFragmentEvents.initFragmentsEvents(this,pcFilesListAdapter);
    }
}
