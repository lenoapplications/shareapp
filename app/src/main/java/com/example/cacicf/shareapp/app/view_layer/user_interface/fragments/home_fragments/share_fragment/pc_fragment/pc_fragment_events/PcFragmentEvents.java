package com.example.cacicf.shareapp.app.view_layer.user_interface.fragments.home_fragments.share_fragment.pc_fragment.pc_fragment_events;

import android.view.View;
import android.widget.ImageButton;

import com.example.cacicf.shareapp.R;
import com.example.cacicf.shareapp.app.android_manager.components_manager.ComponentsManager;
import com.example.cacicf.shareapp.app.view_layer.user_interface.fragments.home_fragments.share_fragment.pc_fragment.PcFilesListAdapter;
import com.example.cacicf.shareapp.app.view_layer.user_interface.fragments.home_fragments.share_fragment.pc_fragment.SharePcFragment;
import com.example.cacicf.shareapp.components.components_configuration.socket_installer_config.client.Client;

import async_communicator.AsyncCommunicator;
import async_communicator.thread_id_holder.ThreadIdHolder;

import static protocol.statics.class_fixed_messages.FileTransferFixedMessages.CLOSE_FOLDER;
import static protocol.statics.class_notification_protocol.FileTransferNotificationProtocol.CLASS_IDENT_PC;
import static protocol.statics.class_notification_protocol.FileTransferNotificationProtocol.PC_STORAGE_CLOSE_FOLDER_REQUEST;

public class PcFragmentEvents {

    public void initFragmentsEvents(SharePcFragment sharePcFragment, PcFilesListAdapter pcFilesListAdapter){
        setupActionBarListeners(sharePcFragment,pcFilesListAdapter);
    }

    private void setupActionBarListeners(SharePcFragment sharePcFragment,PcFilesListAdapter pcFilesListAdapter){
        ImageButton goBackButton = sharePcFragment.getView().findViewById(R.id.homeApp_activity_shareFragment_pcFragment_filesPreview_layout_list_actionBarLayout_imageButton_goBack);

        goBackButton.setOnClickListener(goBackViewOnClickListener(pcFilesListAdapter));
    }

    private View.OnClickListener goBackViewOnClickListener(PcFilesListAdapter pcFilesListAdapter){
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ComponentsManager componentsManager = ComponentsManager.getComponentsManager();
                AsyncCommunicator asyncCommunicator = AsyncCommunicator.getAsyncCommunicator();
                Client client = componentsManager.getClient();

                ThreadIdHolder idHolder = client.sendMessageToServer(CLASS_IDENT_PC,PC_STORAGE_CLOSE_FOLDER_REQUEST,CLOSE_FOLDER);
                String newFilesAndFolders = asyncCommunicator.waitThreadForResponse(idHolder.getThreadId());
                pcFilesListAdapter.replaceCurrentFilesWithNew(newFilesAndFolders);


            }
        };
    }
}
