package com.example.cacicf.shareapp.app.view_layer.user_interface.fragments.home_fragments.share_fragment.pc_fragment.pc_fragment_events.pc_files_list_event;

import android.util.Log;
import android.view.View;

import com.example.cacicf.shareapp.app.android_manager.components_manager.ComponentsManager;
import com.example.cacicf.shareapp.app.view_layer.user_interface.fragments.home_fragments.share_fragment.pc_fragment.PcFilesListAdapter;
import com.example.cacicf.shareapp.components.components_configuration.socket_installer_config.client.Client;
import com.example.cacicf.shareapp.models.data_objects_holder.recylcer_view_objects.home_app_lists.share_fragment_lists.FileSystemObjectsHolder;

import async_communicator.AsyncCommunicator;
import async_communicator.thread_id_holder.ThreadIdHolder;
import protocol.statics.class_fixed_messages.ConnectionFixedMessages;

import static protocol.statics.class_fixed_messages.FileTransferFixedMessages.OPEN_FOLDER;
import static protocol.statics.class_notification_protocol.FileTransferNotificationProtocol.CLASS_IDENT_PC;
import static protocol.statics.class_notification_protocol.FileTransferNotificationProtocol.PC_STORAGE_OPEN_FOLDER_REQUEST;

public class PcFilesListEvents {


    public void initPcFilesListEvents(PcFilesListAdapter pcFilesListAdapter, View cardView, FileSystemObjectsHolder fileSystemObjectsHolder){
        clickOnCardEvent(pcFilesListAdapter,cardView,fileSystemObjectsHolder);
    }


    private void clickOnCardEvent(PcFilesListAdapter pcFilesListAdapter,View cardView,FileSystemObjectsHolder fileSystemObjectsHolder){
        if (fileSystemObjectsHolder.isDirectory()){
            clickOnFolderEvent(pcFilesListAdapter,cardView,fileSystemObjectsHolder);
        }else{
            clickOnFileEvent(cardView,fileSystemObjectsHolder);
        }
    }

    private void clickOnFolderEvent(PcFilesListAdapter pcFilesListAdapter,View cardView,FileSystemObjectsHolder fileSystemObjectsHolder){
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ComponentsManager componentsManager = ComponentsManager.getComponentsManager();
                AsyncCommunicator asyncCommunicator = AsyncCommunicator.getAsyncCommunicator();
                Client client = componentsManager.getClient();
                String folderToOpen = String.format(OPEN_FOLDER,fileSystemObjectsHolder.getFileName());

                ThreadIdHolder idHolder = client.sendMessageToServer(CLASS_IDENT_PC,PC_STORAGE_OPEN_FOLDER_REQUEST,folderToOpen);
                String newFilesAndFolders = asyncCommunicator.waitThreadForResponse(idHolder.getThreadId());
                pcFilesListAdapter.replaceCurrentFilesWithNew(newFilesAndFolders);
            }
        });

    }

    private void clickOnFileEvent(View cardView,FileSystemObjectsHolder fileSystemObjectsHolder){
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }
}
