package com.example.cacicf.shareapp.app.view_layer.user_interface.fragments.home_fragments.share_fragment.phone_fragment;

import com.example.cacicf.shareapp.R;
import com.example.cacicf.shareapp.app.android_manager.view_manager.view_size_handler.font_size_handler.FontSizeHandler;
import com.example.cacicf.shareapp.app.android_manager.view_manager.view_size_handler.widget_size_handler.WidgetSizeHandler;
import com.example.cacicf.shareapp.app.android_manager.view_manager.view_size_handler.window_size_handler.WindowSizeHandler;
import com.example.cacicf.shareapp.models.android_models.abstracts.android_manager.view_manager.fragment_view.fragment_v4_view.FragmentView;

public class SharePhoneFragment extends FragmentView {

    public SharePhoneFragment(){
        super("SHARE_PHONE_FRAGMENT", R.id.homeApp_activity_shareFragment_frame_layout);
    }
    @Override
    public void onCreateFragment() {
        mainAsyncDisplayHandler.setCurrentlyActiveFragmentView(this);
    }

    @Override
    public int onCreateFragmentView() {
        return R.layout.home_app_activity_fragment_share_fragment_phone_phones;
    }

    @Override
    public void onDismissFragmentView() {
        mainAsyncDisplayHandler.setCurrentlyActiveFragmentView(null);
    }

    @Override
    public void setupView(FontSizeHandler fontSizeHandler, WidgetSizeHandler widgetSizeHandler, WindowSizeHandler windowSizeHandler) {

    }
}
