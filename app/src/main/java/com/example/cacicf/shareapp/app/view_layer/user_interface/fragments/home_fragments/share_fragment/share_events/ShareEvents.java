package com.example.cacicf.shareapp.app.view_layer.user_interface.fragments.home_fragments.share_fragment.share_events;

import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.cacicf.shareapp.R;
import com.example.cacicf.shareapp.app.android_manager.components_manager.ComponentsManager;
import com.example.cacicf.shareapp.app.view_layer.user_interface.fragments.home_fragments.share_fragment.ShareFragment;
import com.example.cacicf.shareapp.app.view_layer.user_interface.fragments.home_fragments.share_fragment.pc_fragment.SharePcFragment;
import com.example.cacicf.shareapp.app.view_layer.user_interface.fragments.home_fragments.share_fragment.phone_fragment.SharePhoneFragment;
import com.example.cacicf.shareapp.models.android_models.abstracts.android_manager.view_manager.fragment_view.fragment_v4_view.FragmentView;

import java.io.File;

import async_communicator.AsyncCommunicator;
import async_communicator.thread_id_holder.ThreadIdHolder;
import protocol.statics.class_fixed_messages.FileTransferFixedMessages;
import protocol.statics.class_notification_protocol.FileTransferNotificationProtocol;

import static protocol.statics.class_fixed_messages.FileTransferFixedMessages.CONNECT_PC_STORAGE;
import static protocol.statics.class_notification_protocol.FileTransferNotificationProtocol.CLASS_IDENT_PC;
import static protocol.statics.class_notification_protocol.FileTransferNotificationProtocol.CONNECT_TO_PC_STORAGE_REQUEST;

public class ShareEvents {


    public ShareEvents(){

    }

    public void initFragmentEvents(ShareFragment shareFragment){
        setupHeadBarEvents(shareFragment);
    }

    private void setupHeadBarEvents(ShareFragment shareFragment){
        Button pcButton = shareFragment.getView().findViewById(R.id.homeApp_activity_shareFragment_head_bar_button_pc);
        Button phoneButton = shareFragment.getView().findViewById(R.id.homeApp_activity_shareFragment_head_bar_button_phone);

        pcButton.setOnClickListener(pcButtonViewOnClickListener(shareFragment));
        phoneButton.setOnClickListener(phoneButtonViewOnClickListener(shareFragment));
    }

    private View.OnClickListener pcButtonViewOnClickListener(ShareFragment shareFragment){
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AsyncCommunicator asyncCommunicator = AsyncCommunicator.getAsyncCommunicator();
                ComponentsManager componentsManager = ComponentsManager.getComponentsManager();
                ThreadIdHolder idHolder = componentsManager.getClient().sendMessageToServer(CLASS_IDENT_PC,
                                                                                    CONNECT_TO_PC_STORAGE_REQUEST,
                                                                                    CONNECT_PC_STORAGE);

                String rootFolder = asyncCommunicator.waitThreadForResponse(idHolder.getThreadId());
                SharePcFragment sharePcFragment = FragmentView.createInstance(new Object[]{rootFolder},SharePcFragment.class,null);
                sharePcFragment.startFragmentTransaction(shareFragment.getActivity().getSupportFragmentManager());
            }
        };
    }

    private View.OnClickListener phoneButtonViewOnClickListener(ShareFragment shareFragment){
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharePhoneFragment sharePhoneFragment = new SharePhoneFragment();
                sharePhoneFragment.startFragmentTransaction(shareFragment.getActivity().getSupportFragmentManager());
            }
        };
    }

}
