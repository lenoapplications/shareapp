package com.example.cacicf.shareapp.components.components_configuration.socket_installer_config.client;


import com.example.cacicf.shareapp.app.service_layer.notification_classes.connection_notifications.ConnectionNotifications;
import com.example.cacicf.shareapp.app.service_layer.notification_classes.files_notifications.FilesTransferNotificationPc;
import com.example.cacicf.shareapp.app.service_layer.notification_classes.user_notifications.UserNotificationMethods;
import com.example.cacicf.shareapp.components.components_configuration.socket_installer_config.client.client_socket.ClientSocket;
import com.example.cacicf.shareapp.components.components_configuration.socket_installer_config.client.external_context.ExternalContextInitializator;
import com.example.cacicf.shareapp.components.components_configuration.socket_installer_config.client.notificationer.ClientNotificationer;
import com.example.cacicf.shareapp.components.components_implementation.thread_watcher_implementation.thread_caller.AndroidThreadCaller;
import com.example.cacicf.shareapp.components.components_implementation.thread_watcher_implementation.thread_classes.socket_thread_classes.connection_threads.SocketConnectionThreads;
import com.example.cacicf.shareapp.components.components_implementation.thread_watcher_implementation.thread_classes.socket_thread_classes.data_transfer_threads.DataTransferThreads;
import async_communicator.thread_id_holder.ThreadIdHolder;
import socket_installer.SI_behavior.interfaces.notification.DataTradeModel;
import socket_installer.SI_context.external_context.ExternalContext;

public class Client {

    private ClientSocket clientSocket;


    public ThreadIdHolder configureSocket(String host, int port, int timeout){
        clientSocket = new ClientSocket();
        return clientSocket.configure(host,port,timeout,initClientNotificationer());
    }

    public boolean isConnectedToServer(){
        return clientSocket.getClientCreatedSocket().isConnectedToServer();
    }
    public void disconnectFromServer(){
        AndroidThreadCaller.callThread(SocketConnectionThreads.class,"disconnectFromServerThread",clientSocket.getClientCreatedSocket());
    }

    public ThreadIdHolder sendMessageToServer(String classIdent, String methodIdent, String dataToSend){
        return AndroidThreadCaller.callThreadAndReturnId(DataTransferThreads.class,"sendMessageToServerThread",classIdent,methodIdent,dataToSend,clientSocket.getClientCreatedSocket());

    }

    public ClientNotificationer getClientNotificationer(){
        return (ClientNotificationer) clientSocket.getClientCreatedSocket().getClient().getNotificationer();
    }
    public ExternalContext getExternalContext(){
        return clientSocket.getClientCreatedSocket().getClient().getNotificationer().getExternalContext();
    }
    public boolean isClientInitialized(){
        return clientSocket.isClientInitialized();
    }


    private ClientNotificationer initClientNotificationer(){
        ExternalContextInitializator contextInitializator = new ExternalContextInitializator();
        DataTradeModel[] dataTradeModels = setupDataTradeModels();
        return new ClientNotificationer(dataTradeModels,contextInitializator);
    }


    private DataTradeModel[] setupDataTradeModels(){
        return new DataTradeModel[]{
                new UserNotificationMethods(),
                new FilesTransferNotificationPc(),
                new ConnectionNotifications(),
        };
    }
}
