package com.example.cacicf.shareapp.components.components_configuration.socket_installer_config.client.client_socket;



import com.example.cacicf.shareapp.components.components_implementation.thread_watcher_implementation.thread_caller.AndroidThreadCaller;
import com.example.cacicf.shareapp.components.components_implementation.thread_watcher_implementation.thread_classes.socket_thread_classes.connection_threads.SocketConnectionThreads;
import async_communicator.thread_id_holder.ThreadIdHolder;
import socket_installer.SI_behavior.abstractClasses.notification.notificationer_actions.NotificationerActions;
import socket_installer.SI_behavior.abstractClasses.sockets.created_socket.client.ClientCreatedSocket;


public class ClientSocket {
    private boolean clientInitialized;
    private ClientCreatedSocket clientCreatedSocket;


    public ThreadIdHolder configure(String host, int port, int timeout, NotificationerActions notificationerActions) {
        return AndroidThreadCaller.callThreadAndReturnId(SocketConnectionThreads.class,"configureSocket",this,host,port,timeout,notificationerActions);
    }

    public ClientCreatedSocket getClientCreatedSocket() {
        return clientCreatedSocket;
    }

    public void setClientCreatedSocket(ClientCreatedSocket clientCreatedSocket){
        this.clientCreatedSocket = clientCreatedSocket;
        this.clientInitialized = true;
    }

    public boolean isClientInitialized() {
        return clientInitialized;
    }
}
