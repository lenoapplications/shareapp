package com.example.cacicf.shareapp.components.components_configuration.socket_installer_config.client.external_context;

import android.os.Environment;

import java.io.File;

import file_manager.manager.FileManager;
import file_manager.object_forms.models.user_implementation_models.root.RootInitializationModel;
import socket_installer.SI_context.context_object.ContextObject;
import socket_installer.SI_context.external_context.ExternalContext;

public class ExternalContextInitializator implements socket_installer.SI_behavior.interfaces.context.ExternalContextInitializator {
    @Override
    public void initializeExternalContext(ExternalContext externalContext) {
        try {
            externalContext.saveContextObject(createFileManagerContextObject());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private ContextObject createFileManagerContextObject() throws Exception {
        FileManager fileManager = new FileManager();
        fileManager.initFileManager(new RootInitializationModel() {
            @Override
            public File[] initializationOfRootFiles() {
                return Environment.getExternalStorageDirectory().listFiles();
            }

            @Override
            public String getRootIdentification() {
                return Environment.getExternalStorageDirectory().getAbsolutePath();
            }
        });
        ContextObject contextObject = new ContextObject();
        contextObject.setContextObject(fileManager);
        return contextObject;
    }
}
