package com.example.cacicf.shareapp.components.components_implementation.thread_watcher_implementation.thread_classes.method_processor;

import com.example.cacicf.shareapp.models.components_models.thread_watcher.thread_classes.method_processor.MethodProcessorModel;

import thread_watcher.models.abstractions.user_method.UserMethod;
import thread_watcher.models.annotations.ThreadMethod;
import thread_watcher.user_parts.thread_bundle.Bundle;

public class MethodProcessor extends UserMethod {
    private final String methodProcessorModel = "methodProcessorModel";

    @ThreadMethod(paramNames = {methodProcessorModel})
    public void processMethod(Bundle bundle){
        MethodProcessorModel method = (MethodProcessorModel) bundle.getArguments(methodProcessorModel);
        method.process(Thread.currentThread().getId());

    }
}
