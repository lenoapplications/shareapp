package com.example.cacicf.shareapp.components.components_implementation.thread_watcher_implementation.thread_classes.socket_thread_classes.connection_threads;



import android.widget.EditText;
import android.widget.Toast;
import com.example.cacicf.shareapp.R;
import com.example.cacicf.shareapp.app.android_manager.view_manager.async_display_handler.MainAsyncDisplayHandler;
import com.example.cacicf.shareapp.app.view_layer.user_interface.activites.start_app_activity.ServerListAdapter;
import com.example.cacicf.shareapp.app.view_layer.user_interface.fragments.dialog_fragments.add_server_dialog_fragment.AddServerDialogFragment;
import com.example.cacicf.shareapp.components.components_configuration.socket_installer_config.client.client_socket.ClientSocket;
import com.example.cacicf.shareapp.models.android_models.interfaces.android_manager.view_manager.view_setup_model.main_async_display_handler.HandlerRunMethod;
import com.example.cacicf.shareapp.models.data_objects_holder.recylcer_view_objects.start_app_server_list.DataServerObjectHolder;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

import async_communicator.AsyncCommunicator;
import protocol.statics.class_notification_protocol.ConnectionNotificationProtocol;
import socket_installer.SI.socket_creation.client.ClientSocketCreator;
import socket_installer.SI_behavior.abstractClasses.notification.notificationer_actions.NotificationerActions;
import socket_installer.SI_behavior.abstractClasses.sockets.created_socket.client.ClientCreatedSocket;
import socket_installer.SI_behavior.abstractClasses.sockets.socket_managers.error_manager.exceptions.SocketExceptions;
import thread_watcher.models.abstractions.user_method.UserMethod;
import thread_watcher.models.annotations.ThreadMethod;
import thread_watcher.user_parts.thread_bundle.Bundle;


public class SocketConnectionThreads extends UserMethod {
    private final String clientSocketParam = "clientSocket";
    private final String hostParam = "host";
    private final String portParam = "port";
    private final String timeoutParam = "timeout";
    private final String notificationerActionsParam = "notificationerActions";
    private final String clientCreatedSocketParam = "clientCreatedSocket";


    @ThreadMethod(paramNames = {clientSocketParam,hostParam,portParam,timeoutParam,notificationerActionsParam})
    public void configureSocket(Bundle bundle){
        final int connectTimeout = 5000;
        ClientSocket clientSocket = (ClientSocket) bundle.getArguments(clientSocketParam);
        String host =(String) bundle.getArguments(hostParam);
        Integer port = (Integer) bundle.getArguments(portParam);
        Integer timeout = (Integer) bundle.getArguments(timeoutParam);
        NotificationerActions notificationerActions = (NotificationerActions) bundle.getArguments(notificationerActionsParam);
        AsyncCommunicator asyncCommunicator = AsyncCommunicator.getAsyncCommunicator();

        try {
            Socket socket = new Socket();
            socket.connect(new InetSocketAddress(host,port),connectTimeout);

            ClientCreatedSocket clientCreatedSocket = ClientSocketCreator.createClientCreatedSocket(notificationerActions,socket,timeout);
            clientSocket.setClientCreatedSocket(clientCreatedSocket);
            clientCreatedSocket.initSocket();
            clientCreatedSocket.runSocket(ConnectionNotificationProtocol.CLASS_IDENT,ConnectionNotificationProtocol.CHECK_IF_SERVER_ONLINE_REQUEST,"");
            clientSocket.setClientCreatedSocket(clientCreatedSocket);

        } catch (IOException exception) {
            asyncCommunicator.addResponseToCurrentThread(exception.getClass().getSimpleName());
        } catch (SocketExceptions socketExceptions) {
            asyncCommunicator.addResponseToCurrentThread(socketExceptions.getClass().getSimpleName());
        }
        finally {
            asyncCommunicator.addParameterizedObject("TEST_RESPONSE_THREAD_FINISH",new Object());
        }
    }




    @ThreadMethod(paramNames = {clientCreatedSocketParam})
    public void disconnectFromServerThread(Bundle bundle) throws IOException, SocketExceptions {
        //OVDJE TREBA NAPISATI ALGORITAM

    }
}
