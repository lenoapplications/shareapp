package com.example.cacicf.shareapp.models.android_models.abstracts.android_manager.view_manager.activity_view;

import android.content.Context;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.cacicf.shareapp.app.android_manager.components_manager.ComponentsManager;
import com.example.cacicf.shareapp.app.android_manager.view_manager.ViewManager;
import com.example.cacicf.shareapp.app.android_manager.view_manager.async_display_handler.MainAsyncDisplayHandler;
import com.example.cacicf.shareapp.models.android_models.interfaces.android_manager.view_manager.view_setup_model.activity_setup_model.ViewActivitySetupModel;

public abstract class ActivityView extends AppCompatActivity implements ViewActivitySetupModel {
    protected static ComponentsManager componentsManager;
    private static ViewManager viewManager;
    private static  MainAsyncDisplayHandler mainAsyncDisplayHandler;


    public ActivityView(){
        componentsManager = ComponentsManager.getComponentsManager();
        viewManager = ViewManager.getViewManager();
        mainAsyncDisplayHandler = MainAsyncDisplayHandler.getMainAsyncDisplayHandler();
        mainAsyncDisplayHandler.setCurrentlyActiveActivity(this);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        onCreateActivity();
        viewManager.setupView(this);
    }

    @Override
    public View onCreateView(View parent, String name, Context context, AttributeSet attrs) {
        mainAsyncDisplayHandler.setCurrentlyActiveActivity(this);
        return super.onCreateView(parent, name, context, attrs);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mainAsyncDisplayHandler.setCurrentlyActiveActivity(null);
    }


    public ComponentsManager getComponentsManager() {
        return componentsManager;
    }
}
