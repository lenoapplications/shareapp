package com.example.cacicf.shareapp.models.android_models.abstracts.android_manager.view_manager.adapter_view.recycler_view;


import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.cacicf.shareapp.app.android_manager.components_manager.ComponentsManager;
import com.example.cacicf.shareapp.app.android_manager.view_manager.ViewManager;
import com.example.cacicf.shareapp.models.android_models.abstracts.android_manager.view_manager.activity_view.ActivityView;
import com.example.cacicf.shareapp.models.android_models.interfaces.android_manager.view_manager.view_setup_model.recycler_setup_model.RecylcerViewAdapterModel;

import java.util.List;

public abstract class RecyclerAdapterView <I,H extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<H> implements RecylcerViewAdapterModel{
    protected static  ComponentsManager componentsManager;
    private static  ViewManager viewManager;

    private List<I> items;
    protected View cardViewTemplate;

    public RecyclerAdapterView(List<I> items){
        initItems(items);
        componentsManager = ComponentsManager.getComponentsManager();
        viewManager = ViewManager.getViewManager();
    }

    //ITEMS METHOD
    public void initItems(List<I> items){
        this.items = items;
    }
    public void updateItems(List<I> items){
        changeItemsList(items,true);
    }
    public void addItem(I item){
        items.add(item);
        notifyItemInserted(items.size()-1);
    }
    public void removeItem(int position){
        items.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position,items.size());
    }
    public I getItem(int position){
        return items.get(position);
    }

    public void clearItemList(){
        items.clear();
        notifyDataSetChanged();
    }
    private void changeItemsList(List<I> items,boolean notifyChanges){
        if (items == null){
            throw new IllegalArgumentException("Cannot set 'null' item to the adapter");
        }
        this.items.clear();
        this.items.addAll(items);

        if (notifyChanges){
            notifyDataSetChanged();
        }
    }
    public List<I> getCompleteList(){
        return items;
    }
    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public void onBindViewHolder(@NonNull H holder, int position){
        onBindRecylcerView(holder,position);
    }

    @NonNull
    @Override
    public H onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        cardViewTemplate = layoutInflater.inflate(onCreateCardView(),parent,false);
        viewManager.setupView(this);
        return (H) getViewHolder();
    }

    public static void createRecyclerViewAdapter(AppCompatActivity activityView, View view, RecyclerAdapterView recyclerAdapterView, int resourceId){
        RecyclerView recyclerView = view.findViewById(resourceId);
        LinearLayoutManager manager=new LinearLayoutManager(activityView);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),manager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);

        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(recyclerAdapterView);

    }
}
