package com.example.cacicf.shareapp.models.android_models.abstracts.android_manager.view_manager.fragment_view.dialog_fragment_view;

import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.cacicf.shareapp.app.android_manager.components_manager.ComponentsManager;
import com.example.cacicf.shareapp.app.android_manager.view_manager.ViewManager;
import com.example.cacicf.shareapp.app.android_manager.view_manager.async_display_handler.MainAsyncDisplayHandler;
import com.example.cacicf.shareapp.models.android_models.interfaces.android_manager.view_manager.view_setup_model.fragment_setup_model.ViewFragmentSetupModel;

import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public abstract class DialogFragmentView extends DialogFragment implements ViewFragmentSetupModel {
    protected static  ComponentsManager componentsManager;
    private static  ViewManager viewManager;
    protected static  MainAsyncDisplayHandler mainAsyncDisplayHandler;

    protected final String tag;

    public DialogFragmentView(String tag){
        this.tag = tag;
        componentsManager = ComponentsManager.getComponentsManager();
        viewManager = ViewManager.getViewManager();
        mainAsyncDisplayHandler = MainAsyncDisplayHandler.getMainAsyncDisplayHandler();
    }


    public static <A extends DialogFragmentView> A createInstance(Object[] bundleObjects,Class<A> dialogInstance,Object[] constructorArgs,Class<? extends Object>...argsClass){

        A createdInstance = setupInstanceConstructor(dialogInstance,constructorArgs,argsClass);
        if(bundleObjects != null){
            createBundle(bundleObjects,createdInstance);
        }
        return createdInstance;
    }
    private static<A extends DialogFragmentView> void createBundle(Object[] bundleObjects,A createdInstance){
        Bundle bundle = new Bundle();
        for (Object object : bundleObjects){
            bundle.putSerializable(object.getClass().getSimpleName(), (Serializable) object);
        }
        createdInstance.setArguments(bundle);
    }
    private static <A extends DialogFragmentView> A setupInstanceConstructor(Class<A> dialogInstance, Object[] constructorArgs,Class<? extends Object> ... argsClass){
        try {
            Constructor<A> constructor = dialogInstance.getConstructor(argsClass);
            return constructor.newInstance(constructorArgs);
        } catch (NoSuchMethodException | IllegalAccessException | java.lang.InstantiationException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View mainView = inflater.inflate(onCreateFragmentView(),null,false);
        return mainView;
    }

    @Override
    public void onResume() {
        super.onResume();
        viewManager.setupView(this);
        onCreateFragment();
    }


    public void show(FragmentManager manager){
        super.show(manager,tag);
    }

}
