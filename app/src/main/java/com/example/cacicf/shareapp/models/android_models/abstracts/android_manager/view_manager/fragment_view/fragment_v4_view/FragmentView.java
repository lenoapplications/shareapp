package com.example.cacicf.shareapp.models.android_models.abstracts.android_manager.view_manager.fragment_view.fragment_v4_view;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.cacicf.shareapp.app.android_manager.components_manager.ComponentsManager;
import com.example.cacicf.shareapp.app.android_manager.view_manager.ViewManager;
import com.example.cacicf.shareapp.app.android_manager.view_manager.async_display_handler.MainAsyncDisplayHandler;
import com.example.cacicf.shareapp.models.android_models.abstracts.android_manager.view_manager.fragment_view.dialog_fragment_view.DialogFragmentView;
import com.example.cacicf.shareapp.models.android_models.interfaces.android_manager.view_manager.view_setup_model.fragment_setup_model.ViewFragmentSetupModel;

import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public abstract class FragmentView extends Fragment implements ViewFragmentSetupModel{
    protected static ComponentsManager componentsManager;
    private static ViewManager viewManager;
    protected static MainAsyncDisplayHandler mainAsyncDisplayHandler;

    private final String tag;
    private final int fragmentContainer;

    public FragmentView(String tag,int fragmentContainer){
        this.tag = tag;
        this.fragmentContainer = fragmentContainer;
        componentsManager = ComponentsManager.getComponentsManager();
        viewManager = ViewManager.getViewManager();
        mainAsyncDisplayHandler = MainAsyncDisplayHandler.getMainAsyncDisplayHandler();
    }

    public static <A extends FragmentView> A createInstance(Object[] bundleObjects, Class<A> dialogInstance, Object[] constructorArgs, Class<? extends Object>...argsClass){

        A createdInstance = setupInstanceConstructor(dialogInstance,constructorArgs,argsClass);
        if(bundleObjects != null){
            createBundle(bundleObjects,createdInstance);
        }
        return createdInstance;
    }
    private static<A extends FragmentView> void createBundle(Object[] bundleObjects,A createdInstance){
        Bundle bundle = new Bundle();
        for (Object object : bundleObjects){
            bundle.putSerializable(object.getClass().getSimpleName(), (Serializable) object);
        }
        createdInstance.setArguments(bundle);
    }
    private static <A extends FragmentView> A setupInstanceConstructor(Class<A> fragmentInstance, Object[] constructorArgs,Class<? extends Object> ... argsClass){
        try {
            Constructor<A> constructor = fragmentInstance.getConstructor(argsClass);
            return constructor.newInstance(constructorArgs);
        } catch (NoSuchMethodException | IllegalAccessException | java.lang.InstantiationException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View mainView = inflater.inflate(onCreateFragmentView(),null,false);
        return mainView;
    }

    @Override
    public void onResume() {
        super.onResume();
        viewManager.setupView(this);
        onCreateFragment();
    }


    public void startFragmentTransaction(FragmentManager fragmentManager){
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.replace(fragmentContainer,this,tag);
        fragmentTransaction.commit();
    }
}
