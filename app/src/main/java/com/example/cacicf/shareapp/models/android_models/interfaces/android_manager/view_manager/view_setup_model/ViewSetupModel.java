package com.example.cacicf.shareapp.models.android_models.interfaces.android_manager.view_manager.view_setup_model;


import com.example.cacicf.shareapp.app.android_manager.view_manager.view_size_handler.font_size_handler.FontSizeHandler;
import com.example.cacicf.shareapp.app.android_manager.view_manager.view_size_handler.widget_size_handler.WidgetSizeHandler;
import com.example.cacicf.shareapp.app.android_manager.view_manager.view_size_handler.window_size_handler.WindowSizeHandler;

import java.io.Serializable;

public interface ViewSetupModel extends Serializable {
    void setupView(FontSizeHandler fontSizeHandler, WidgetSizeHandler widgetSizeHandler, WindowSizeHandler windowSizeHandler);
}
