package com.example.cacicf.shareapp.models.android_models.interfaces.android_manager.view_manager.view_setup_model.activity_setup_model;


import android.os.Bundle;

import com.example.cacicf.shareapp.models.android_models.interfaces.android_manager.view_manager.view_setup_model.ViewSetupModel;

public interface ViewActivitySetupModel extends ViewSetupModel{
    void onCreateActivity();
}
