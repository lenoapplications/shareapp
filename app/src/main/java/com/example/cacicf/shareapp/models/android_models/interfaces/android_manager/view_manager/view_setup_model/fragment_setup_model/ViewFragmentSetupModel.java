package com.example.cacicf.shareapp.models.android_models.interfaces.android_manager.view_manager.view_setup_model.fragment_setup_model;



import com.example.cacicf.shareapp.models.android_models.interfaces.android_manager.view_manager.view_setup_model.ViewSetupModel;

public interface ViewFragmentSetupModel extends ViewSetupModel {
    void onCreateFragment();
    int onCreateFragmentView();
    void onDismissFragmentView();
}
