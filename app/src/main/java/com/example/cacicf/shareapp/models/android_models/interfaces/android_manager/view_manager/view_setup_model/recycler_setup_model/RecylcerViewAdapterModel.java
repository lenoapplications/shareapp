package com.example.cacicf.shareapp.models.android_models.interfaces.android_manager.view_manager.view_setup_model.recycler_setup_model;

import android.support.v7.widget.RecyclerView;

import com.example.cacicf.shareapp.models.android_models.interfaces.android_manager.view_manager.view_setup_model.ViewSetupModel;

public interface RecylcerViewAdapterModel <H extends RecyclerView.ViewHolder> extends ViewSetupModel{
    void onBindRecylcerView(H holder,int position);
    int onCreateCardView();
    H getViewHolder();

}
