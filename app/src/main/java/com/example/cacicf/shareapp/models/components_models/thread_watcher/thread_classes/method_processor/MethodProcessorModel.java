package com.example.cacicf.shareapp.models.components_models.thread_watcher.thread_classes.method_processor;

public interface MethodProcessorModel {


    void process(long threadId);

}
