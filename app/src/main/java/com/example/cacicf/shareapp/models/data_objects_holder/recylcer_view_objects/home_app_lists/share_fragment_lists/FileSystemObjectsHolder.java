package com.example.cacicf.shareapp.models.data_objects_holder.recylcer_view_objects.home_app_lists.share_fragment_lists;

public class FileSystemObjectsHolder {
    private final String fileName;
    private final boolean isDirectory;

    public FileSystemObjectsHolder(String fileName) {
        isDirectory = fileName.charAt(0) == '<';
        this.fileName = fileName.substring(1);
    }

    public String getFileName() {
        return fileName;
    }

    public boolean isDirectory() {
        return isDirectory;
    }
}
