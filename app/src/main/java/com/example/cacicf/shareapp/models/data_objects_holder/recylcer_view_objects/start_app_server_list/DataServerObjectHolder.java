package com.example.cacicf.shareapp.models.data_objects_holder.recylcer_view_objects.start_app_server_list;

public class DataServerObjectHolder {
    private final String serverName;
    private final String serverIp;
    private boolean serverStatus;
    private boolean deleteServer;

    public DataServerObjectHolder(String serverName,String serverIp,boolean serverStatus){
        this.serverName = serverName;
        this.serverIp = serverIp;
        this.serverStatus = serverStatus;
        this.deleteServer = false;
    }

    public String getServerIp() {
        return serverIp;
    }

    public String getServerName() {
        return serverName;
    }

    public boolean getServerStatus() {
        return serverStatus;
    }

    public boolean isDeleteServer() {
        return deleteServer;
    }

    public void setDeleteServer(boolean deleteServer) {
        this.deleteServer = deleteServer;
    }
}
